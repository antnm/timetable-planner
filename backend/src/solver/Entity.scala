package timetableplanner
package solver

import scala.jdk.CollectionConverters.given
import java.util.UUID
import eu.timepit.refined.auto.given
import org.optaplanner.core.api.domain.lookup.PlanningId
import org.optaplanner.core.api.domain.entity.PlanningEntity
import org.optaplanner.core.api.domain.variable.PlanningVariable
import org.optaplanner.core.api.domain.solution.ProblemFactCollectionProperty
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider
import entities.Schedule
import entities.Entity.{
  Class,
  Classroom,
  Subject,
  Teacher,
  RequiredLengthConstraint,
  RequiredClassroomConstraint,
  RequiredSubjectConstraint,
  RequiredSubjectGroupConstraint,
  AlignmentConstraint
}

@PlanningEntity
private class Entity(
    schedule: Schedule,
    val `class`: (Class, Int),
    val subject: Subject,
    val required: RequiredSubjectConstraint | RequiredSubjectGroupConstraint
):
  private val idVal = UUID.randomUUID

  @PlanningId
  private def id = idVal

  private val teachersVal = schedule
    .collect {
      case (teacher: Teacher) if teacher.subjects.exists(_ == subject) =>
        teacher
    }
    .toSet
    .asJava

  @ValueRangeProvider(id = "teacherRange")
  @ProblemFactCollectionProperty
  def teachers = teachersVal

  private val classroomsVal = {
    val required = schedule
      .collect {
        case (required: RequiredClassroomConstraint)
            if required.subjects contains subject =>
          required.classrooms
      }
    if required.isEmpty then Vector(Option.empty[Classroom])
    else required.reduce(_ ++ _).distinct.map(Some(_))
  }.asJava

  @ValueRangeProvider(id = "classroomRange")
  def classrooms = classroomsVal

  val length = schedule.collectFirst {
    case required: RequiredLengthConstraint
        if required.subjects.exists(_ == subject) =>
      required.length: Int
  } getOrElse 60

  private val minutesVal =
    val stored = schedule
      .collect {
        case alignment: AlignmentConstraint
            if alignment.subjects.exists(_ == subject) =>
          alignment
      }
      .flatMap(alignment =>
        alignment.onTheDaysOf.flatMap(day =>
          (day * 24 * 60 + alignment.interval._1.asMinutes)
            .to(day * 24 * 60 + alignment.interval._2.asMinutes, alignment.step)
            .filter(
              _ - day * 24 * 60 + length <= alignment.interval._2.asMinutes
            )
        )
      )
      .toSet
    (if stored.isEmpty then (0.to(24 * 60 * 7, length).toSet)
     else stored).map(Integer(_)).asJava

  @ValueRangeProvider(id = "minuteRange")
  @ProblemFactCollectionProperty
  def minutes = minutesVal

  private val startingFromWeeksVal = (required match
    case r: RequiredSubjectGroupConstraint if r.spansMultipleWeeks =>
      (1 to r.subjects.length).toSet
    case r: RequiredSubjectConstraint =>
      (1 to r.weekFrequency).toSet
    case _ => Set(1)
  ).map(int2Integer).asJava

  @ValueRangeProvider(id = "startingFromWeekRange")
  @ProblemFactCollectionProperty
  def startingFromWeeks = startingFromWeeksVal

  val weekFrequency = (required match
    case r: RequiredSubjectGroupConstraint if r.spansMultipleWeeks =>
      r.subjects.length
    case r: RequiredSubjectConstraint => r.weekFrequency: Int
    case _                            => 1
  )

  @PlanningVariable(valueRangeProviderRefs = Array("teacherRange"))
  val teacher: Teacher = null

  @PlanningVariable(valueRangeProviderRefs = Array("classroomRange"))
  val classroom: Option[Classroom] = null

  @PlanningVariable(valueRangeProviderRefs = Array("minuteRange"))
  val minute: Integer = null

  @PlanningVariable(valueRangeProviderRefs = Array("startingFromWeekRange"))
  val startingFromWeek: Integer = null

  def this() = this(Schedule(), null, null, null)

  override def toString =
    s"Entity(${`class`}, $subject, $teacher, $classroom, $length, $minute, $startingFromWeek)"
