package timetableplanner
package service

import scala.util.Try
import cats.effect.kernel.Async
import cats.implicits.given
import org.http4s.{HttpDate, HttpRoutes}
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.{`If-Modified-Since`, `Last-Modified`}
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder

private def messagesRoute[F[_]: Async](messages: Messages) =
  val lastModified =
    HttpDate.fromEpochSecond(BuildInfo.lastModifiedMessages.toLong).toOption.get
  val dsl = Http4sDsl[F]
  import dsl.*
  HttpRoutes.of[F] { case req @ GET -> Root / "api" / "messages" / localeName =>
    Try(SupportedLocale.valueOf(localeName)).toOption match
      case Some(locale) =>
        req.headers.get[`If-Modified-Since`] match
          case Some(header) if header.date >= lastModified =>
            NotModified()
          case _ =>
            Ok(messages(locale))
              .map(_.putHeaders(`Last-Modified`(lastModified)))
      case None => NotFound()
  }
