package timetableplanner
package db

import scala.util.Try
import scala.concurrent.duration.FiniteDuration
import java.sql.Timestamp
import java.util.Base64
import cats.effect.kernel.{Async, Clock}
import cats.effect.std.Random
import cats.implicits.*
import doobie.ConnectionIO
import doobie.implicits.*
import doobie.implicits.javasql.*

class Session private (
    private val bytes: Array[Byte]
):
  def code = Base64.getEncoder.encodeToString(bytes)

  def use[F[_]: Async](
      random: Random[F],
      config: Config
  ): F[ConnectionIO[Option[(User, Session, Boolean)]]] =
    for
      newBytes <- random.nextBytes(config.session.codeSize)
      now <- Clock[F].realTime
    yield sql"""
        update sessions
        set expiration = ${Session.expiration(now, config)},
        code = $newBytes
        from users
        where users.id = sessions.user_id
        and sessions.code = $bytes
        and sessions.expiration > ${Timestamp(now.toMillis)}
        returning users.id, users.name, users.password, sessions.keep_logged_in
      """
      .query[(User, Boolean)]
      .option
      .map(
        _.map((user, keepLoggedIn) =>
          (user, new Session(newBytes), keepLoggedIn)
        )
      )

object Session:
  def generate[F[_]: Async](
      user: User,
      keepLoggedIn: Boolean,
      random: Random[F],
      config: Config
  ): F[ConnectionIO[Session]] =
    for
      code <- random.nextBytes(256)
      now <- Clock[F].realTime
    yield sql"""
      insert into sessions (user_id, expiration, code, keep_logged_in)
      values (${user.id}, ${expiration(now, config)}, $code, $keepLoggedIn)
    """.update.run.fmap(_ => new Session(code))

  def apply(code: String, config: Config) =
    Try(Base64.getDecoder.decode(code)).toOption.map(new Session(_))

  private def expiration(now: FiniteDuration, config: Config) = Timestamp(
    now.plus(config.session.expiration).toMillis
  )

  def removeExpired[F[_]: Async](): F[ConnectionIO[Unit]] =
    scribe.debug("Removing expired sessions")
    Clock[F].realTime.map(now => sql"""
        delete from sessions
        where expiration <= ${Timestamp(now.toMillis)}
      """.update.run.fmap(_ => ()))
