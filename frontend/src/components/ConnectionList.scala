package timetableplanner
package components

import scala.scalajs.js.{Array, Dictionary}
import java.util.UUID
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.Refined
import com.raquo.laminar.api.L.{*, given}
import entities.{Schedule, Entity, Named}

class ConnectionList[A <: Entity & Named](collect: PartialFunction[Entity, A])
    extends EntityComponent[Vector[A]]:
  def show(uuid: UUID, s: Signal[Schedule], connections: Vector[A])(using
      Signal[Map[String, String]]
  ) = Vector(
    span(
      children <-- s.map(s =>
        Intl
          .ListFormat(documentLang, Dictionary.empty)
          .formatToParts(
            Array(connections.map(_.name: String)*)
          )
          .foldLeft((0, Vector[Child]())) {
            case ((index, acc), el) if el("type") == "literal" =>
              (index, acc :+ el("value"))
            case ((index, acc), el)
                if nameInput.duplicate(s, Refined.unsafeApply(el("value"))) =>
              (
                index + 1,
                acc :+ span(
                  el("value"),
                  " (",
                  `var`(connections(index).uuid.toString),
                  ")"
                )
              )
            case ((index, acc), el) => (index + 1, acc :+ el("value"))
          }
          ._2
      )
    )
  )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val current = Var(Vector[A]())
    val clear = EventBus[Unit]()
    val selected = EventBus[Vector[A]]
    val elem = ul(
      clear.events.mapTo(Vector()) --> current,
      selected.events --> current,
      children <-- s
        .map(_.collect(collect))
        .split(_.uuid)((uuid, _, signal) =>
          li(
            label(
              input(
                typ := "checkbox",
                checked <-- current.signal.map(_.exists(_.uuid == uuid)),
                composeEvents(onChange.mapToChecked)(
                  _.withCurrentValueOf(signal, current).map(
                    (checked, entity, current) =>
                      if checked then current.appended(entity)
                      else current.filterNot(_.uuid == entity.uuid)
                  )
                ) --> current
              ),
              children <-- signal
                .combineWith(s)
                .map((c, s) =>
                  if nameInput.duplicate(s, c.name) then
                    Vector(c.name: String, " (", `var`(uuid.toString), ")")
                  else Vector(c.name: String)
                )
            )
          )
        )
    )
    EntityComponent.Editable(
      Vector(elem),
      current.signal.map(Some(_)),
      clear.toObserver,
      selected.toObserver
    )

given subjectConnectionsList: EntityComponent[Vector[Entity.Subject]] =
  new ConnectionList({ case e: Entity.Subject => e })
given teacherConnectionsList: EntityComponent[Vector[Entity.Teacher]] =
  new ConnectionList({ case e: Entity.Teacher => e })
given classConnectionsList: EntityComponent[Vector[Entity.Class]] =
  new ConnectionList({ case e: Entity.Class => e })
given classroomConnectionsList: EntityComponent[Vector[Entity.Classroom]] =
  new ConnectionList({ case e: Entity.Classroom => e })
