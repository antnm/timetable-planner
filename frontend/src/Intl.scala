package timetableplanner

import scala.scalajs.js
import scala.scalajs.js.{Dictionary, Iterable, Array, Date, Object, Any}
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
object Intl extends Object:
  @js.native
  class DisplayNames(locales: String, options: Dictionary[Any]) extends Object:
    def of(code: String): String = js.native

  @js.native
  class ListFormat(locales: String, options: Dictionary[Any]) extends Object:
    def format(list: Iterable[String]): String = js.native
    def formatToParts(list: Iterable[String]): Array[Dictionary[String]] =
      js.native

  @js.native
  class DateTimeFormat(locales: String, options: Dictionary[Any])
      extends Object:
    def format(date: Date): String = js.native

  @js.native
  class NumberFormat(locales: String, options: Dictionary[Any]) extends Object:
    def format(number: Int): String = js.native
