package timetableplanner

import eu.timepit.refined.api.Refined
import cats.effect.kernel.{Async, Ref}
import cats.implicits.given
import doobie.implicits.given
import doobie.util.transactor.Transactor
import fs2.{Chunk, Stream}, fs2.concurrent.Topic
import solver.Solver
import events.{Event, Name}
import entities.{
  Schedule,
  Timetable,
  State,
  TimetableState,
  schedules,
  updateSchedules
}

class Planner[F[_]: Async] private (
    xa: Transactor[F],
    config: Config,
    eventTopic: Topic[F, (Planner.PatchId, Event)],
    dbEventTopic: Topic[F, (Planner.PatchId, Planner.DbEvent)],
    stateRef: Ref[F, (Planner.PatchId, State)]
):
  def patch(event: Event.Patch): F[Unit] = for
    tuple <- stateRef.modify((patchId, state) =>
      val nextId = patchId + 1
      val schedules = state.schedules
      val patched = event.patch(schedules)
      (
        (nextId, state.updateSchedules(patched)),
        (nextId, event, schedules, patched)
      )
    )
    (id, event, schedules, patched) = tuple
    _ <- publishEvent(
      id,
      event,
      Planner.DbEvent.UpdatedSchedules(schedules, patched)
    )
  yield ()

  def schedules = stateRef.get.map(_._2.schedules)

  def solve(name: String Refined Name): F[Boolean] = for
    state <- stateRef.get.map(_._2)
    result <-
      if state
          .get(name)
          .exists(_.timetable match
            case TimetableState.Solving => false
            case _                      => true
          )
      then
        val schedules = state.schedules
        for
          id <- stateRef.modify((patchId, state) =>
            val nextId = patchId + 1
            (
              (
                nextId,
                state.updatedWith(name)(
                  _.map(_.copy(timetable = TimetableState.Solving))
                )
              ),
              nextId
            )
          )
          _ <- publishEvent(
            id,
            Event.Solving(name),
            Planner.DbEvent.SetTimetable(name, None)
          )
          timetable = Solver(config).solve(schedules(name))
          id <- stateRef.modify((patchId, state) =>
            val nextId = patchId + 1
            (
              (
                nextId,
                state.updatedWith(name)(
                  _.map(
                    _.copy(timetable = timetable match
                      case Some(timetable) =>
                        TimetableState.Solved(timetable, published = false)
                      case None => TimetableState.None
                    )
                  )
                )
              ),
              nextId
            )
          )
          _ <- publishEvent(
            id,
            Event.Solved(name, timetable),
            Planner.DbEvent.SetTimetable(name, timetable)
          )
        yield true
      else Async[F].pure(false)
  yield result

  def setPublished(name: String Refined Name, published: Boolean): F[Boolean] =
    for
      schedules <- schedules
      result <-
        if schedules contains name then
          for
            id <- stateRef.modify((patchId, state) =>
              val nextId = patchId + 1
              (
                (
                  nextId,
                  state.updatedWith(name)(
                    _.map(e =>
                      e.copy(timetable = e.timetable match
                        case timetable: TimetableState.Solved =>
                          timetable.copy(published = published)
                        case timetable => timetable
                      )
                    )
                  )
                ),
                nextId
              )
            )
            _ <- publishEvent(
              id,
              Event.Published(name, published),
              Planner.DbEvent.Published(name, published)
            )
          yield true
        else Async[F].pure(false)
    yield result

  // Events from a topic might come in a different order than the one
  // they were sent in, so we need to sort them by their patch id
  private def orderEvents[A](
      firstId: Int,
      stream: Stream[F, (Planner.PatchId, A)]
  ) = stream
    .mapAccumulate((firstId, Seq.empty[(Planner.PatchId, A)])) {
      case ((expected, prevEvents), received) =>
        val events = (prevEvents :+ received).sortBy(_._1)
        val toSend = Chunk.seq(
          events.zipWithIndex
            .takeWhile { case ((patchId, _), index) =>
              expected + index == patchId
            }
            .map { case ((_, event), _) => event }
        )
        ((expected + toSend.size, events.drop(toSend.size)), toSend)
    }
    .map { case (_, toSend) => toSend }
    .unchunks

  def events = stateRef.get.map { case (patchId, state) =>
    orderEvents(
      patchId,
      Stream((patchId, Event.Initial(state))) ++ eventTopic.subscribe(
        config.topic.eventsMaxQueue
      )
    )
  }

  def dbUpdates = orderEvents(
    Int.MinValue + 1,
    dbEventTopic.subscribe(config.topic.dbUpdatesMaxQueue)
  )
    .foreach {
      case Planner.DbEvent.UpdatedSchedules(old, patched) =>
        (old.keySet
          .diff(patched.keySet)
          .map(db.Schedule.remove(_).transact(xa))
          .toSeq ++ {
          val schedulesSet = old.toSet
          patched.map {
            case schedule @ (name, content)
                if !(schedulesSet contains schedule) =>
              db.Schedule.setSchedule(name, content).transact(xa)
            case _ => Async[F].pure(())
          }
        }.toSeq).sequence_
      case Planner.DbEvent.SetTimetable(name, timetable) =>
        db.Schedule.updateTimetable(name, timetable).transact(xa)
      case Planner.DbEvent.Published(name, published) =>
        db.Schedule.updatePublished(name, published).transact(xa)
    }

  def publishedTimetable(name: String Refined Name): F[Option[Timetable]] =
    stateRef.get.map(
      _._2
        .get(name)
        .flatMap(_.timetable match
          case TimetableState.Solved(timetable, true) => Some(timetable)
          case _                                      => None
        )
    )

  private def publishEvent(
      patchId: Planner.PatchId,
      event: Event,
      dbEvent: Planner.DbEvent
  ) = for
    eventPublish <- eventTopic.publish1((patchId, event))
    _ = if eventPublish.isLeft then scribe.error("Event topic closed")
    dbEventPublish <- dbEventTopic.publish1((patchId, dbEvent))
    _ = if dbEventPublish.isLeft then scribe.error("DB event topic closed")
    else ()
  yield ()

object Planner:
  private type PatchId = Int

  private enum DbEvent:
    case UpdatedSchedules(
        old: Map[String Refined Name, Schedule],
        patched: Map[String Refined Name, Schedule]
    )
    case SetTimetable(name: String Refined Name, timetable: Option[Timetable])
    case Published(name: String Refined Name, published: Boolean)

  def apply[F[_]: Async](xa: Transactor[F], config: Config) = for
    eventTopic <- Topic[F, (PatchId, Event)]
    dbEventTopic <- Topic[F, (PatchId, DbEvent)]
    state <- db.Schedule.getAll.transact(xa).map((Int.MinValue, _))
    stateRef <- Ref.of(state)
    planner = new Planner(xa, config, eventTopic, dbEventTopic, stateRef)
  yield planner
