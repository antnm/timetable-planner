package timetableplanner
package service

import cats.effect.kernel.Async
import cats.effect.std.Random
import cats.implicits.given
import eu.timepit.refined.auto.given
import doobie.implicits.given
import doobie.util.transactor.Transactor
import org.http4s.{HttpRoutes, Response, ResponseCookie, SameSite}
import org.http4s.dsl.Http4sDsl
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import io.circe.Codec
import db.{Invite, Session, User}
import events.{LoginRequest, RegisterRequest}

private def authenticationRoutes[F[_]: Async](
    xa: Transactor[F],
    random: Random[F],
    config: Config
) =
  Codec[LoginRequest]
  Codec[RegisterRequest]

  val dsl = Http4sDsl[F]
  import dsl.*
  HttpRoutes.of[F] {
    case req @ POST -> Root / "api" / "login" =>
      for
        req <- req.as[LoginRequest]
        user <- User.login(req).transact(xa)
        response <- user match
          case Some(user) =>
            for
              resp <- Ok()
              sessionGen <- Session
                .generate(user, req.keepLoggedIn, random, config)
              session <- sessionGen.transact(xa)
            yield resp.addCookie(
              Authenticator.cookie(config, session, req.keepLoggedIn)
            )
          case None => Response(Unauthorized).pure[F]
      yield response

    case req @ POST -> Root / "api" / "register" =>
      for
        req <- req.as[RegisterRequest]
        invite <- Invite(req.code) match
          case Some(invite) =>
            invite.isValid.flatMap(
              _.transact(xa).fmap(if _ then Some(invite) else None)
            )
          case None => None.pure[F]
        response <- invite match
          case Some(invite) =>
            User.register(req.name, req.password).transact(xa).flatMap {
              case Some(user) =>
                for
                  _ <- invite.remove().transact(xa)
                  resp <- Ok()
                yield resp
              case None => Response(Conflict).pure[F]
            }
          case None => Response(Unauthorized).pure[F]
      yield response
  }
