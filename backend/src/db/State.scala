package timetableplanner
package db

import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto.given
import doobie.{ConnectionIO, Fragments}
import doobie.implicits.given
import doobie.postgres.circe.jsonb.implicits.given
import io.circe.{Json, Decoder}
import io.circe.syntax.given
import entities.{Schedule, Timetable, State, TimetableState, StateEntry, given}
import events.Name

object Schedule:
  def getAll: ConnectionIO[State] = sql"""
    select name, content, timetable, published from schedules
  """
    .query[(String, Json, Option[Json], Boolean)]
    .to[Vector]
    .map(
      _.map((name, schedule, timetable, published) =>
        (
          Refined.unsafeApply[String, Name](name),
          StateEntry(
            summon[Decoder[Schedule]].decodeJson(schedule).toOption.get,
            timetable.map(timetable =>
              TimetableState.Solved(
                summon[Decoder[Timetable]].decodeJson(timetable).toOption.get,
                published
              )
            ) getOrElse TimetableState.None
          )
        )
      ).toMap
    )

  def setSchedule(
      name: String Refined Name,
      schedule: Schedule
  ): ConnectionIO[Unit] = sql"""
    insert into schedules (name, content)
    values (${name: String}, ${schedule.asJson})
    on conflict (name) do update set content = excluded.content
  """.update.run.map(_ => ())

  def updateTimetable(
      name: String Refined Name,
      timetable: Option[Timetable]
  ): ConnectionIO[Unit] = sql"""
    update schedules set timetable = ${timetable.map(_.asJson)}
    where name = ${name: String}
  """.update.run.map(_ => ())

  def updatePublished(
      name: String Refined Name,
      published: Boolean
  ): ConnectionIO[Unit] = sql"""
    update schedules set published = $published
    where name = ${name: String}
  """.update.run.map(_ => ())

  def remove(name: String Refined Name): ConnectionIO[Unit] = sql"""
    delete from schedules
    where name = ${name: String}
  """.update.run.map(_ => ())
