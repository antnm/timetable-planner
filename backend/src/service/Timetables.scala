package timetableplanner
package service

import scala.annotation.targetName
import java.time.{LocalTime, DayOfWeek}
import java.time.format.{FormatStyle, TextStyle, DateTimeFormatter}
import java.text.NumberFormat
import java.util.Locale
import eu.timepit.refined.refineV
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto.given
import cats.effect.kernel.Async
import cats.implicits.given
import org.http4s.{HttpRoutes, LanguageTag, MediaType}
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Content-Type`
import scalatags.Text.all.*
import scalatags.Text.Tag
import scalatags.Text.tags2.{title, nav, main}
import entities.{Timetable, LocaleFormatter, TimeOfDay, DayOfTheWeek}
import events.Name

class JvmLocaleFormatter(locale: SupportedLocale) extends LocaleFormatter:
  private val javaLocale = Locale(locale.toString)

  def format(time: TimeOfDay) = LocalTime
    .of(time.hour, time.minute)
    .format(
      DateTimeFormatter
        .ofLocalizedTime(FormatStyle.SHORT)
        .withLocale(javaLocale)
    )

  def format(number: Int) = NumberFormat.getInstance(javaLocale).format(number)

  @targetName("formatDay")
  def format(day: Int Refined DayOfTheWeek) =
    DayOfWeek.of(day + 1).getDisplayName(TextStyle.FULL, javaLocale)

def renderPage(
    name: String Refined Name,
    locale: SupportedLocale,
    messages: Messages,
    currentGroup: Option[String],
    tags: Tag*
) =
  doctype("html")(
    html(
      lang := locale.toString,
      meta(charset := "utf-8"),
      link(rel := "stylesheet", href := "/common.css"),
      link(rel := "icon", href := "/icon.svg"),
      title(s"Timetable Planner - $name"),
      body(
        header(
          nav(
            Seq("subjects", "classes", "teachers", "classrooms").map(group =>
              a(
                href := s"/timetables/$name/$group",
                currentGroup.filter(_ == group).map(_ => cls := "current"),
                messages(locale)(group)
              )
            )
          ),
          h1(a(href := s"/timetables/$name", name: String))
        ),
        main(tags)
      )
    )
  ).render

private def renderTable(
    name: String Refined Name,
    locale: SupportedLocale,
    messages: Messages,
    timetable: Timetable,
    group: String,
    filter: (Timetable.ShowFilter, String)
) = renderPage(
  name,
  locale,
  messages,
  Some(group),
  h2(filter._2),
  Timetable
    .show(
      scalatags.Text,
      new JvmLocaleFormatter(locale),
      messages(locale),
      timetable,
      Some(filter),
      subjectRender = Some(n => a(href := s"/timetables/$name/subjects/$n", n)),
      classRender = Some(n => a(href := s"/timetables/$name/classes/$n", n)),
      teacherRender = Some(n => a(href := s"/timetables/$name/teachers/$n", n)),
      classroomRender =
        Some(n => a(href := s"/timetables/$name/classrooms/$n", n))
    )
)

private def renderList(
    name: String Refined Name,
    locale: SupportedLocale,
    messages: Messages,
    timetable: Timetable,
    group: String,
    filter: Timetable.ShowFilter
) = renderPage(
  name,
  locale,
  messages,
  Some(group),
  h2(messages(locale)(group)),
  ul(
    timetable
      .collect(filter match
        case Timetable.ShowFilter.Class => (e => (e.`class`: String))
        case Timetable.ShowFilter.Classroom =>
          ((e: Timetable.Entry) => e.classroom.map(c => c: String)).unlift
        case Timetable.ShowFilter.Subject => (e => (e.subject: String))
        case Timetable.ShowFilter.Teacher => (e => (e.teacher): String)
      )
      .toSeq
      .sortBy(identity)
      .map(e => li(a(href := s"/timetables/$name/$group/$e", e)))
  )
)

private def tableRoute[F[_]: Async](
    planner: Planner[F],
    messages: Messages,
    entityPath: String,
    filter: Timetable.ShowFilter
) =
  val dsl = Http4sDsl[F]
  import dsl.*
  HttpRoutes.of[F] {
    case req @ GET -> Root / "timetables" / name / group / entity
        if group == entityPath =>
      (refineV[Name](name), refineV[Name](entity)) match
        case (Right(name), Right(entity)) =>
          for
            timetable <- planner.publishedTimetable(name)
            result <- timetable match
              case Some(timetable) =>
                Ok(
                  renderTable(
                    name,
                    extractLocale(req),
                    messages,
                    timetable,
                    group,
                    (filter, entity: String)
                  ),
                  `Content-Type`(MediaType.text.html)
                )
              case None => NotFound()
          yield result
        case _ => NotFound()
  }

private def listRoute[F[_]: Async](
    planner: Planner[F],
    messages: Messages,
    entityPath: String,
    filter: Timetable.ShowFilter
) =
  val dsl = Http4sDsl[F]
  import dsl.*
  HttpRoutes.of[F] {
    case req @ GET -> Root / "timetables" / name / group
        if group == entityPath =>
      refineV[Name](name) match
        case Right(name) =>
          for
            timetable <- planner.publishedTimetable(name)
            result <- timetable match
              case Some(timetable) =>
                Ok(
                  renderList(
                    name,
                    extractLocale(req),
                    messages,
                    timetable,
                    entityPath,
                    filter
                  ),
                  `Content-Type`(MediaType.text.html)
                )
              case None => NotFound()
          yield result
        case _ => NotFound()
  }

private def mainRoute[F[_]: Async](planner: Planner[F], messages: Messages) =
  val dsl = Http4sDsl[F]
  import dsl.*
  HttpRoutes.of[F] { case req @ GET -> Root / "timetables" / name =>
    refineV[Name](name) match
      case Right(name) =>
        for
          timetable <- planner.publishedTimetable(name)
          result <- timetable match
            case Some(timetable) =>
              Ok(
                renderPage(name, extractLocale(req), messages, None),
                `Content-Type`(MediaType.text.html)
              )
            case None => NotFound()
        yield result
      case _ => NotFound()
  }

private def timetableRoutes[F[_]: Async](
    planner: Planner[F],
    messages: Messages
) = mainRoute(planner, messages) <+> Seq(
  ("subjects", Timetable.ShowFilter.Subject),
  ("teachers", Timetable.ShowFilter.Teacher),
  ("classes", Timetable.ShowFilter.Class),
  ("classrooms", Timetable.ShowFilter.Classroom)
).map((path, filter) =>
  tableRoute(planner, messages, path, filter)
    <+> listRoute(planner, messages, path, filter)
).reduce(_ <+> _)
