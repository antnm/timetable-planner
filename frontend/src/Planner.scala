package timetableplanner

import org.scalajs.dom.HttpMethod.PUT
import org.scalajs.dom.EventSource
import java.net.HttpURLConnection.{HTTP_OK, HTTP_BAD_REQUEST}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto.given
import cats.implicits.given
import com.raquo.laminar.api.L.{*, given}
import io.circe.parser.decode
import entities.{
  Schedule,
  Timetable,
  State,
  TimetableState,
  schedules,
  updateSchedules
}
import events.{Event, Patch, Name, given}

class Planner:
  private val stateVar = Var(State())
  private val patchSchedulesBus = EventBus[Patch]()
  private val unconfirmedPatches = Var(Seq[Patch]())
  private val eventBus = EventBus[Event]()
  private val source = Var[Option[EventSource]](None)
  val currentSchedule = Var(Option.empty[String Refined Name])
  val events = eventBus.events
  private val openBus = EventBus[Unit]()
  val open = openBus.events
  private val errorBus = EventBus[Unit]()
  val error = errorBus.events
  val disconnect = Observer[Unit](_ => source.now().map(_.close()))
  val connect = Observer[Unit](_ =>
    source.update {
      case None =>
        val source = EventSource("/api/planner")
        source.onopen = (_ => openBus.writer onNext ())
        source.onerror = (_ => errorBus.writer onNext ())
        source.onmessage = (event =>
          given Map[String Refined Name, Schedule] =
            stateVar.now().schedules
          val decoded = decode[Event](event.data.asInstanceOf[String])
          decoded.left.foreach(err =>
            scribe.error(s"Failed to parse event: ${err.show}")
          )
          val result = decoded.toOption.get
          scribe.debug(s"Received event: $result")
          eventBus.writer onNext result
        )
        Some(source)
      case Some(source) => Some(source)
    }
  )

  private def updateCurrentSchedule(
      patched: Map[String Refined Name, Schedule]
  ) =
    currentSchedule set patched.minByOption(p => (p._1: String)).map(_._1)

  private def patchSchedules(patched: Map[String Refined Name, Schedule]) =
    if currentSchedule.now().filter(patched contains _).isEmpty then
      updateCurrentSchedule(patched)
    stateVar update (_.updateSchedules(patched))

  val patchSchedules = patchSchedulesBus.toObserver

  val schedules = stateVar.signal.map(_.schedules)

  val schedule = schedules.signal
    .combineWithFn(currentSchedule, unconfirmedPatches.signal)(
      (schedules, current, patches) =>
        current.flatMap(name =>
          patches
            .foldLeft(schedules)((schedules, patch) => patch(schedules))
            .get(name)
        ) getOrElse Schedule()
    )

  val timetable =
    stateVar.signal.combineWithFn(currentSchedule)((state, current) =>
      current.flatMap(
        state.get(_).map(_.timetable)
      ) getOrElse TimetableState.None
    )

  val published =
    stateVar.signal.combineWithFn(currentSchedule)((state, current) =>
      current.exists(
        state
          .get(_)
          .exists(_.timetable match
            case TimetableState.Solved(_, published) => published
            case _                                   => false
          )
      )
    )

  def bindings = Seq(
    events.collect { case Event.Initial(state) =>
      updateCurrentSchedule(state.schedules)
      state
    } --> stateVar,
    events.collect { case Event.ConfirmPatch => () } --> (_ =>
      unconfirmedPatches update (patches =>
        patchSchedules(patches.head(stateVar.now().schedules))
        patches drop 1
      )
    ),
    events.collect { case event: Event.Solved => event } --> {
      case Event.Solved(name, timetable) =>
        stateVar update (_.updatedWith(name)(
          _.map(
            _.copy(timetable =
              timetable.map(
                TimetableState.Solved(_, published = false)
              ) getOrElse TimetableState.None
            )
          )
        ))
    },
    events.collect { case event: Event.Solving => event } --> {
      case Event.Solving(name) =>
        stateVar update (_.updatedWith(name)(
          _.map(_.copy(timetable = TimetableState.Solving))
        ))
    },
    events.collect { case event: Event.Published => event } --> {
      case Event.Published(name, published) =>
        stateVar update (_.updatedWith(name)(
          _.map(e =>
            e.copy(timetable = e.timetable match
              case timetable: TimetableState.Solved =>
                timetable.copy(published = published)
              case timetable => timetable
            )
          )
        ))
    },
    events.withCurrentValueOf(stateVar).collect {
      case (Event.Patch(_, patch), state) =>
        val patched = patch(state.schedules)
        patchSchedules(patched)
        state.updateSchedules(patched)
    } --> stateVar,
    patchSchedulesBus.events
      .map(patch =>
        unconfirmedPatches update (_ :+ patch)
        patch
      )
      .flatMap(patch => fetch(PUT, "/api/planner", patch).map((_, patch))) --> (
      (resp, patch) =>
        resp.status match
          case HTTP_BAD_REQUEST =>
            unconfirmedPatches update (_.filterNot(_ eq patch))
          case HTTP_OK => ()
          case _ =>
            unconfirmedPatches update (_.filterNot(_ eq patch))
            scribe.error(
              s"Trying to patch schedule failed with code: ${resp.status}"
            )
    )
  )
