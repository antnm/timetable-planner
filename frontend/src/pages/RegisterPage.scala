package timetableplanner
package pages

import java.net.HttpURLConnection.{HTTP_CONFLICT, HTTP_OK, HTTP_UNAUTHORIZED}
import eu.timepit.refined.auto.given
import eu.timepit.refined.boolean.True
import com.raquo.laminar.api.L.{*, given}
import com.raquo.waypoint.Router
import org.scalajs.dom.HttpMethod.POST
import components.{error, ValidatedInput, given}
import events.{Name, Password, RegisterRequest, UserInfo, given}

def registerPage(using router: Router[Page])(using
    Signal[Map[String, String]]
) =
  val waitingRequest = Var(false)
  val name = ValidatedInput[Name](
    defaultValue = "",
    invalidMessage = "nameRequirements",
    disabled <-- waitingRequest
  )
  val password = ValidatedInput[Password](
    defaultValue = "",
    invalidMessage = "passwordRequirements",
    typ := "password",
    disabled <-- waitingRequest
  )
  val confirmPassword = ValidatedInput[True](
    defaultValue = "",
    invalidMessage = "",
    typ := "password",
    disabled <-- waitingRequest,
    inContext(confirmPassword =>
      Signal
        .combine(
          password.events(onInput).startWith(()).mapTo(password.ref.value),
          confirmPassword
            .events(onInput)
            .startWith(())
            .mapTo(confirmPassword.ref.value)
        )
        .map((password, confirm) =>
          if password != confirm then Some("passwordNotMatching") else None
        ) --> error
    )
  )
  val inviteCode = ValidatedInput[True](
    defaultValue = "",
    invalidMessage = "",
    disabled <-- waitingRequest,
    onInput.mapTo(None) --> error
  )
  val hasError = Signal
    .combineSeq(
      Seq(
        name.error.signal,
        password.error.signal,
        confirmPassword.error.signal,
        inviteCode.error.signal
      )
    )
    .map(_.exists(_.isDefined))

  div(
    cls := "page",
    header(h1(img(src := "/icon.svg", alt := ""), "Timetable Planner")),
    main(
      h2(child.text <-- messages("registerTitle")),
      label(child.text <-- messages("name"), name),
      label(child.text <-- messages("password"), password),
      label(
        child.text <-- messages("confirmPassword"),
        confirmPassword
      ),
      label(child.text <-- messages("invitationCode"), inviteCode),
      button(
        typ := "button",
        child.text <-- messages("register"),
        disabled <-- hasError.combineWithFn(waitingRequest.signal)(_ || _),
        composeEvents(onClick)(
          _.sample(
            name.validated.combineWith(
              password.validated,
              inviteCode.validated
            )
          ).collect { case (Some(name), Some(password), Some(inviteCode)) =>
            (name, password, inviteCode)
          }.flatMap { case (name, password, inviteCode) =>
            waitingRequest set true
            fetch(
              POST,
              "api/register",
              RegisterRequest(name, password, inviteCode)
            )
          }.map(_.status)
        ) --> {
          case HTTP_CONFLICT =>
            scribe.debug("Register name conflict")
            name.error set Some("nameAlreadyInUse")
            name.report onNext ()
            waitingRequest set false
          case HTTP_UNAUTHORIZED =>
            scribe.debug("Unauthorized invitation code")
            inviteCode.error set Some("invalidCode")
            inviteCode.report onNext ()
            waitingRequest set false
          case HTTP_OK =>
            scribe.debug("Registration successful")
            router.pushState(LoginPage)
          case _ =>
            scribe.warn("Unknown registration status code")
            waitingRequest set false
        }
      ),
      a(
        child.text <-- messages("loginExisting"),
        href := "/register",
        onClick.preventDefault.mapTo(LoginPage) --> router.pushState
      )
    )
  )
