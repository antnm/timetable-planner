package timetableplanner
package entities

import java.lang.{Class => JavaClass}
import java.util.UUID
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.{Refined, Validate}
import eu.timepit.refined.numeric.{Positive, NonNegative, Interval}
import io.circe.Codec
import io.circe.refined.given
import events.Name

type Schedule = Vector[Entity]

object Schedule:
  def apply(elems: Entity*): Schedule = Vector(elems*)

type DayOfTheWeek = Interval.ClosedOpen[0, 7]

type Hour = Interval.ClosedOpen[0, 24]

type Minute = Interval.ClosedOpen[0, 60]

case class TimeOfDay(hour: Int Refined Hour, minute: Int Refined Minute)
    derives Codec.AsObject:
  def asMinutes = hour * 60 + minute

case class TimeOfDayInterval()
given Validate.Plain[(TimeOfDay, TimeOfDay), TimeOfDayInterval] =
  Validate.fromPredicate(
    intv =>
      intv._1.hour < intv._2.hour
        || (intv._1.hour == intv._2.hour && intv._1.minute < intv._2.minute),
    intv => s"($intv is a time interval)",
    TimeOfDayInterval()
  )

case class NonNegativeIntRange()
given Validate.Plain[
  (Int Refined NonNegative, Int Refined NonNegative),
  NonNegativeIntRange
] =
  Validate.fromPredicate(
    r => r._1 <= r._2,
    r => s"($r is a non negative int range)",
    NonNegativeIntRange()
  )

sealed trait Named:
  val name: String Refined Name

// The order of the cases is important for decoding. One case may depend on
// previous cases but not the other way around.
enum Entity:
  val uuid: UUID
  case Subject(uuid: UUID = UUID.randomUUID(), name: String Refined Name)
      extends Entity
      with Named
  case Teacher(
      uuid: UUID = UUID.randomUUID(),
      name: String Refined Name,
      subjects: Vector[Subject] = Vector()
  ) extends Entity with Named
  case Class(
      uuid: UUID = UUID.randomUUID(),
      name: String Refined Name,
      count: Int Refined Positive = Refined.unsafeApply(1)
  ) extends Entity with Named
  case Classroom(uuid: UUID = UUID.randomUUID(), name: String Refined Name)
      extends Entity
      with Named
  case AlignmentConstraint(
      uuid: UUID = UUID.randomUUID(),
      interval: (TimeOfDay, TimeOfDay) Refined TimeOfDayInterval,
      step: Int Refined Positive,
      onTheDaysOf: Set[Int Refined DayOfTheWeek],
      subjects: Vector[Subject] = Vector()
  )
  case RequiredLengthConstraint(
      uuid: UUID = UUID.randomUUID(),
      length: Int Refined Positive,
      subjects: Vector[Subject] = Vector()
  )
  case RequiredSubjectConstraint(
      uuid: UUID = UUID.randomUUID(),
      weekFrequency: Int Refined Positive = Refined.unsafeApply(1),
      count: Int Refined Positive = Refined.unsafeApply(1),
      subjects: Vector[Subject] = Vector(),
      classes: Vector[Class] = Vector()
  )
  case RequiredSubjectGroupConstraint(
      uuid: UUID = UUID.randomUUID(),
      count: Int Refined Positive = Refined.unsafeApply(1),
      subjects: Vector[Subject] = Vector(),
      classes: Vector[Class] = Vector(),
      canHappenInTheSameClassroom: Boolean,
      spansMultipleWeeks: Boolean,
      mustHappenAtTheSameTime: Boolean
  )
  case RequiredClassroomConstraint(
      uuid: UUID = UUID.randomUUID(),
      subjects: Vector[Subject] = Vector(),
      classrooms: Vector[Classroom] = Vector()
  )
  case ExcludedTimeIntervalConstraint(
      uuid: UUID = UUID.randomUUID(),
      priority: Int Refined Positive,
      day: Int Refined DayOfTheWeek,
      interval: (TimeOfDay, TimeOfDay) Refined TimeOfDayInterval,
      subjects: Vector[Subject] = Vector(),
      teachers: Vector[Teacher] = Vector(),
      classes: Vector[Class] = Vector(),
      classrooms: Vector[Classroom] = Vector()
  )
  case GapConstraint(
      uuid: UUID = UUID.randomUUID(),
      priority: Int Refined Positive,
      rangeMinutes: (
          Int Refined NonNegative,
          Int Refined NonNegative
      ) Refined NonNegativeIntRange,
      teachers: Vector[Teacher] = Vector(),
      classes: Vector[Class] = Vector(),
      subjects: Vector[Subject] = Vector()
  )
  case TeacherEntryCountRangeConstraint(
      uuid: UUID = UUID.randomUUID(),
      priority: Int Refined Positive,
      range: (
          Int Refined NonNegative,
          Int Refined NonNegative
      ) Refined NonNegativeIntRange,
      teachers: Vector[Teacher] = Vector()
  )
