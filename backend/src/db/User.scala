package timetableplanner
package db

import cats.effect.unsafe.implicits.global
import cats.effect.IO
import cats.implicits.given
import eu.timepit.refined.auto.given
import com.password4j.Password
import doobie.{ConnectionIO, Fragments}
import doobie.implicits.given
import events.{LoginRequest, UserInfo}

case class User private (id: Int, name: String, password: String):
  def changeInfo(info: UserInfo): ConnectionIO[Boolean] =
    if !Password
        .check(info.oldPassword, password)
        .withArgon2
    then false.pure[ConnectionIO]
    else
      (info.name match
        case Some(name) => User.exists(name).map(!_)
        case None       => true.pure[ConnectionIO]
      ).flatMap(nameOk =>
        if !nameOk then false.pure[ConnectionIO]
        else
          val hashedPassword =
            info.password.map(Password.hash(_).withArgon2.getResult)
          (fr"update users" ++ Fragments.setOpt(
            info.name.map(name => fr"name = ${name: String}"),
            hashedPassword.map(password => fr"password = ${password.toString}")
          ) ++ fr"where id = $id").update.run.map(_ => true)
      )

object User:
  def login(req: LoginRequest): ConnectionIO[Option[User]] =
    insertAdminIfNoUserExist().flatMap(_ =>
      sql"""
        select id, name, password
        from users
        where name = ${req.name}
      """
        .query[User]
        .option
        .map(
          _.filter(user =>
            Password.check(req.password, user.password).withArgon2
          )
        )
    )

  def register(name: String, password: String): ConnectionIO[Option[User]] =
    exists(name).flatMap(
      if _ then None.pure[ConnectionIO]
      else
        val hashedPassword = Password.hash(password).withArgon2.getResult
        sql"""
          insert into users (name, password)
          values ($name, $hashedPassword)
          returning id
        """
          .query[Int]
          .unique
          .fmap(id => Some(User(id, name, password)))
    )

  def find(id: Int): ConnectionIO[Option[User]] =
    insertAdminIfNoUserExist().flatMap(_ => sql"""
      select id, name, password
      from users
      where id = $id
    """.query[User].option)

  private def exists(name: String): ConnectionIO[Boolean] =
    sql"""
      select 1
      from users
      where name = $name
    """.query[Int].option.map(_.isDefined)

  private def insertAdminIfNoUserExist(): ConnectionIO[Unit] =
    sql"""select count(*) from users"""
      .query[Int]
      .unique
      .flatMap(count =>
        (if count == 0 then
           scribe.info("No user exists, adding admin")
           val hashedPassword = Password.hash("admin").withArgon2.getResult
           sql"""
             insert into users (name, password)
             values ('admin', $hashedPassword)
           """.update.run.map(_ => ())
         else ().pure[ConnectionIO])
      )
