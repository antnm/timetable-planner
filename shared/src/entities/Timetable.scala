package timetableplanner
package entities

import scala.annotation.targetName
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Positive
import io.circe.Codec
import io.circe.refined.given
import scalatags.generic.Bundle
import events.Name
import scalatags.generic.TypedTag

trait LocaleFormatter:
  def format(time: TimeOfDay): String
  def format(number: Int): String
  @targetName("formatDay")
  def format(day: Int Refined DayOfTheWeek): String

type Timetable = Set[Timetable.Entry]
object Timetable:
  def apply(elems: Entry*): Timetable = Set(elems*)

  case class Entry(
      `class`: String Refined Name,
      teacher: String Refined Name,
      subject: String Refined Name,
      start: TimeOfDay,
      end: TimeOfDay,
      day: Int Refined DayOfTheWeek,
      startingFromWeek: Int Refined Positive = Refined.unsafeApply(1),
      weekFrequency: Int Refined Positive = Refined.unsafeApply(1),
      classroom: Option[String Refined Name] = None
  ) derives Codec.AsObject

  enum ShowFilter:
    case Subject
    case Class
    case Teacher
    case Classroom

  def show[Builder, Output <: FragT, FragT](
      bundle: Bundle[Builder, Output, FragT],
      formatter: LocaleFormatter,
      messages: Map[String, String],
      timetable: Timetable,
      filter: Option[(ShowFilter, String)] = None,
      subjectRender: Option[String => TypedTag[Builder, Output, FragT]] = None,
      classRender: Option[String => TypedTag[Builder, Output, FragT]] = None,
      teacherRender: Option[String => TypedTag[Builder, Output, FragT]] = None,
      classroomRender: Option[String => TypedTag[Builder, Output, FragT]] = None
  ) =
    import bundle.all.*
    def filtered(
        except: ShowFilter,
        tag: => TypedTag[Builder, Output, FragT]
    ) = filter match
      case Some((filter, _)) if filter != except => Some(tag)
      case None                                  => Some(tag)
      case _                                     => None
    table(
      thead(
        tr(
          th(messages("from")),
          th(messages("to")),
          filtered(ShowFilter.Subject, th(messages("subject"))),
          filtered(ShowFilter.Class, th(messages("class"))),
          filtered(ShowFilter.Teacher, th(messages("teacher"))),
          filtered(ShowFilter.Classroom, th(messages("classroom"))),
          th(messages("day")),
          th(messages("week"))
        )
      ),
      tbody(
        timetable.toVector
          .filter(filter match
            case None => (e: Entry) => true
            case Some(ShowFilter.Subject, name) =>
              (e: Entry) => (e.subject: String) == name
            case Some(ShowFilter.Class, name) =>
              (e: Entry) => (e.`class`: String) == name
            case Some(ShowFilter.Teacher, name) =>
              (e: Entry) => (e.teacher: String) == name
            case Some(ShowFilter.Classroom, name) =>
              (e: Entry) => e.classroom.exists(n => (n: String) == name)
          )
          .sortBy(e => e.start.hour * 60 + e.start.minute + e.day * 60 * 24)
          .map(e =>
            tr(
              td(formatter.format(e.start)),
              td(formatter.format(e.end)),
              filtered(
                ShowFilter.Subject,
                td(
                  subjectRender.map(_(e.subject)) getOrElse (e.subject: String)
                )
              ),
              filtered(
                ShowFilter.Class,
                td(classRender.map(_(e.`class`)) getOrElse (e.`class`: String))
              ),
              filtered(
                ShowFilter.Teacher,
                td(
                  teacherRender.map(_(e.teacher)) getOrElse (e.teacher: String)
                )
              ),
              filtered(
                ShowFilter.Classroom,
                td(e.classroom match
                  case Some(c) =>
                    classroomRender.map(_(c)) getOrElse (c: String)
                  case None => ""
                )
              ),
              td(formatter.format(e.day)),
              td {
                val startingFrom = formatter.format(e.startingFromWeek)
                val frequency = formatter.format(e.weekFrequency)
                s"$startingFrom/$frequency"
              }
            )
          )
      )
    )
