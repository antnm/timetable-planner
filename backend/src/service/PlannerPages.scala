package timetableplanner
package service

import cats.effect.kernel.Async
import org.http4s.{HttpRoutes, MediaType}
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Content-Type`
import scalatags.Text.all.*
import scalatags.Text.tags2.title

private def plannerPageRoutes[F[_]: Async] =
  val dsl = Http4sDsl[F]
  import dsl.*
  HttpRoutes.of[F] {
    case req @ (GET -> Root / path)
        if Seq(
          "login",
          "register",
          "planner",
          "user",
          "invites"
        ) contains path =>
      Ok(
        doctype("html")(
          html(
            lang := extractLocale(req).toString,
            meta(charset := "utf-8"),
            link(rel := "stylesheet", href := "/webapp.css"),
            link(rel := "icon", href := "/icon.svg"),
            script(src := "/out.js"),
            title("Timetable Planner"),
            body()
          )
        ).render,
        `Content-Type`(MediaType.text.html)
      )
  }
