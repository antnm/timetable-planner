package timetableplanner
package solver

import scala.jdk.CollectionConverters.given
import eu.timepit.refined.auto.given
import org.optaplanner.core.api.domain.solution.{
  PlanningEntityCollectionProperty,
  PlanningScore,
  PlanningSolution,
  ProblemFactCollectionProperty
}
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore
import entities.Schedule
import entities.Entity.{
  RequiredSubjectConstraint,
  RequiredSubjectGroupConstraint,
  ExcludedTimeIntervalConstraint,
  GapConstraint,
  Teacher,
  TeacherEntryCountRangeConstraint
}

@PlanningSolution
private class Solution(schedule: Schedule):
  @PlanningEntityCollectionProperty
  val entries = schedule
    .collect {
      case required: RequiredSubjectConstraint =>
        val classes = required.classes
          .flatMap(`class` => (1 to `class`.count) map ((`class`, _)))
        val subjects = (1 until required.count).foldLeft(required.subjects)(
          (acc, _) => acc ++ required.subjects
        )
        classes.flatMap(`class` =>
          subjects.map(Entity(schedule, `class`, _, required))
        )
      case required: RequiredSubjectGroupConstraint =>
        val classes = required.classes
          .flatMap(`class` => (1 to `class`.count) map ((`class`, _)))
        val subjects = (1 until required.count).foldLeft(required.subjects)(
          (acc, _) => acc ++ required.subjects
        )
        classes.flatMap(`class` =>
          subjects.map(Entity(schedule, `class`, _, required))
        )
    }
    .flatten
    .toSeq
    .asJava

  @ProblemFactCollectionProperty
  val excludedTimeIntervals = schedule
    .collect { case excluded: ExcludedTimeIntervalConstraint =>
      excluded
    }
    .toSeq
    .asJava

  @ProblemFactCollectionProperty
  val maximumGaps =
    schedule
      .collect { case maximumGap: GapConstraint => maximumGap }
      .toSeq
      .asJava

  @ProblemFactCollectionProperty
  val teachers =
    schedule.collect { case teacher: Teacher => teacher }.toSeq.asJava

  @ProblemFactCollectionProperty
  val teacherEntryCountRanges = schedule
    .collect { case range: TeacherEntryCountRangeConstraint => range }
    .toSeq
    .asJava

  @PlanningScore
  val score: HardSoftScore = null

  def this() = this(Schedule())

  override def toString =
    s"Schedule($entries, $score})"
