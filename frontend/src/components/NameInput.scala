package timetableplanner
package components

import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.Refined
import entities.{Schedule, Named}
import events.Name

given nameInput: EntityComponent[String Refined Name] with
  def duplicate(s: Schedule, n: String Refined Name) =
    s.count {
      case o: Named if o.name == n => true
      case _                       => false
    } > 1

  def show(uuid: UUID, s: Signal[Schedule], n: String Refined Name)(using
      Signal[Map[String, String]]
  ) = Vector(
    span(
      n: String,
      children <-- s.map(s =>
        if duplicate(s, n) then Vector(" (", `var`(uuid.toString), ")")
        else Vector()
      )
    )
  )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val input = ValidatedInput[Name]("", "nameRequirements")
    EntityComponent.Editable(
      Seq(input),
      input.validated,
      input.clear,
      Observer(n => input.value set n)
    )
