package timetableplanner
package components

import scala.util.Try
import scala.scalajs.js.Dictionary
import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.{Refined, Validate}
import eu.timepit.refined.numeric.Positive
import eu.timepit.refined.string.ValidInt
import entities.Schedule

case class ValidPositiveInt()
given Validate.Plain[String, ValidPositiveInt] =
  Validate.fromPredicate(
    str => Try(str.toInt).toOption.filter(_ > 0).isDefined,
    str => s"$str is a valid positive int",
    ValidPositiveInt()
  )

given positiveIntInput: EntityComponent[Int Refined Positive] with
  def show(uuid: UUID, s: Signal[Schedule], n: Int Refined Positive)(using
      Signal[Map[String, String]]
  ) = Vector(
    span(
      Intl
        .NumberFormat(documentLang, Dictionary.empty)
        .format(n)
    )
  )
  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val input = ValidatedInput[ValidPositiveInt](
      "",
      "positiveIntRequirement",
      typ := "number"
    )
    EntityComponent.Editable(
      Seq(input),
      input.validated.map(_.map(v => Refined.unsafeApply((v: String).toInt))),
      input.clear,
      Observer(s => input.value set s.toString)
    )
