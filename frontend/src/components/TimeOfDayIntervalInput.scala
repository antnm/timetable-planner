package timetableplanner
package components

import scala.util.Try
import scala.scalajs.js.{Dictionary, Date}
import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.refineV
import eu.timepit.refined.api.{Refined, Validate}
import entities.{Schedule, TimeOfDay, TimeOfDayInterval, given}

given timeOfDayIntervalInput
    : EntityComponent[(TimeOfDay, TimeOfDay) Refined TimeOfDayInterval] with
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      i: (TimeOfDay, TimeOfDay) Refined TimeOfDayInterval
  )(using Signal[Map[String, String]]) = Vector(
    span(i.toList.map(t => timeOfDayInput.format(t)).reduce(_ ++ "-" ++ _))
  )

  def unsafeExtract(s: String) =
    s match
      case timeRegex(hour, minute) =>
        TimeOfDay(
          Refined.unsafeApply(hour.toInt),
          Refined.unsafeApply(minute.toInt)
        )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val start =
      ValidatedInput[ValidTimeOfDay]("", "timeRequirement", typ := "time")
    val end =
      ValidatedInput[ValidTimeOfDay]("", "timeRequirement", typ := "time")
    EntityComponent.Editable(
      Seq(div(cls := "interval", start, "-", end)),
      start.validated.combineWithFn(end.validated) {
        case (Some(start), Some(end)) =>
          refineV[TimeOfDayInterval](
            (unsafeExtract(start), unsafeExtract(end))
          ).toOption
        case _ => None
      },
      Observer(_ =>
        start.clear onNext ()
        end.clear onNext ()
      ),
      Observer(i =>
        start.value set "%02d:%02d".format(i._1.hour, i._1.minute)
        end.value set "%02d:%02d".format(i._2.hour, i._2.minute)
      )
    )
