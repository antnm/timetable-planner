package timetableplanner.components

import scala.scalajs.js
import org.scalajs.dom
import com.raquo.laminar.api.L.{*, given}
import com.raquo.laminar.nodes.ReactiveHtmlElement
import com.raquo.domtypes.generic.codecs.{BooleanAsIsCodec, StringAsIsCodec}

object dialog:
  @js.native
  trait RawElement extends js.Object:
    def close(returnValue: String): Unit
    def show(): Unit
    def showModal(): Unit

  type Ref = dom.html.Element with RawElement

  val open = customProp("open", BooleanAsIsCodec)
  val returnValue = customProp("returnValue", StringAsIsCodec)

  val cancel = customEventProp("cancel")
  val close = customEventProp("close")

  def apply(
      mods: Modifier[ReactiveHtmlElement[Ref]]*
  ): ReactiveHtmlElement[Ref] =
    customHtmlTag[Ref]("dialog")(mods)
