import mill._, mill.scalajslib._, scalalib._
import scala.concurrent.duration._
import $ivy.`com.lihaoyi::mill-contrib-buildinfo:`
import mill.contrib.buildinfo.BuildInfo
import $ivy.`com.lihaoyi::mill-contrib-flyway:`
import contrib.flyway.FlywayModule
import $ivy.`com.lihaoyi::mill-contrib-bloop:`

object Deps {
  val scalaVersion = "3.1.1"
  val scalaJSVersion = "1.8.0"
  val refined = ivy"eu.timepit::refined::0.9.28"
  object circe {
    val version = "0.15.0-M1"
    val core = ivy"io.circe::circe-core::$version"
    val generic = ivy"io.circe::circe-generic::$version"
    val parser = ivy"io.circe::circe-parser::$version"
    val refined = ivy"io.circe::circe-refined::$version"
    val scalajs = ivy"io.circe::circe-scalajs::$version"
  }
  val optaplanner = ivy"org.optaplanner:optaplanner-core:8.17.0.Final"
  object scribe {
    val version = "3.7.1"
    val root = ivy"com.outr::scribe::$version"
    val slf4j = ivy"com.outr::scribe-slf4j:$version"
  }
  val pureconfig = ivy"com.github.pureconfig::pureconfig-core:0.17.1"
  object http4s {
    val version = "1.0.0-M31"
    val emberServer = ivy"org.http4s::http4s-ember-server:$version"
    val circe = ivy"org.http4s::http4s-circe:$version"
    val dsl = ivy"org.http4s::http4s-dsl:$version"
  }
  val postgresql = ivy"org.postgresql:postgresql:42.3.2"
  object doobie {
    val version = "1.0.0-RC1"
    val core = ivy"org.tpolecat::doobie-core:$version"
    val postgres = ivy"org.tpolecat::doobie-postgres:$version"
    val postgresCirce = ivy"org.tpolecat::doobie-postgres-circe:$version"
  }
  val scalatags = ivy"com.lihaoyi::scalatags::0.11.1"
  val password4j = ivy"com.password4j:password4j:1.5.4"
  val scalaJSDom = ivy"org.scala-js::scalajs-dom::2.1.0"
  val laminar = ivy"com.raquo::laminar::0.14.2"
  val waypoint = ivy"com.raquo::waypoint::0.5.0"
  val munit = ivy"org.scalameta::munit:1.0.0-M1"
}

object backend extends ScalaModule with BuildInfo with FlywayModule {
  override def scalaVersion = Deps.scalaVersion
  override def moduleDeps = Seq(shared)
  override def ivyDeps = Agg(
    Deps.circe.core,
    Deps.circe.generic,
    Deps.circe.parser,
    Deps.optaplanner,
    Deps.scribe.root,
    Deps.scribe.slf4j,
    Deps.pureconfig,
    Deps.http4s.emberServer,
    Deps.http4s.circe,
    Deps.http4s.dsl,
    Deps.postgresql,
    Deps.doobie.core,
    Deps.doobie.postgres,
    Deps.doobie.postgresCirce,
    Deps.password4j
  )
  private def cssFiles = T.sources {
    os.walk(millSourcePath / "stylesheets")
      .filter(_.ext == "css")
      .map(PathRef(_))
  }

  private def lastModifiedMessages = T.input {
    os.walk(millSourcePath / "resources")
      .filter(path =>
        path.ext == "properties" && (path.baseName startsWith "Messages")
      )
      .map(_.toIO.lastModified.milliseconds.toSeconds)
      .reduce(_ max _)
  }

  def cleanCss = T {
    cssFiles() foreach { file =>
      os.proc("cleancss", file.path.toString, "--output", file.path.last)
        .call(cwd = T.dest)
    }
    PathRef(T.dest)
  }

  override def resources = T.sources {
    super.resources() ++ Seq(
      cleanCss(),
      PathRef(frontend.fastOpt().path / os.up)
    )
  }

  override def buildInfoPackageName = Some("timetableplanner")
  override def buildInfoMembers = T {
    Map(
      "lastModifiedMessages" -> lastModifiedMessages().toString
    )
  }

  override def flywayUrl = "jdbc:postgresql://db:5432/timetableplanner"
  override def flywayDriverDeps = Agg(Deps.postgresql)
  override def flywayUser = "timetableplanner"
  override def flywayPassword = "timetableplanner"

  object test extends Tests with TestModule.Munit {
    override def scalaVersion = Deps.scalaVersion
    override def ivyDeps = Agg(Deps.munit)
  }
}

object frontend extends ScalaJSModule {
  override def scalaVersion = Deps.scalaVersion
  override def scalaJSVersion = Deps.scalaJSVersion
  override def moduleDeps = Seq(shared)
  override def ivyDeps = Agg(
    Deps.circe.core,
    Deps.circe.generic,
    Deps.circe.parser,
    Deps.circe.scalajs,
    Deps.scalaJSDom,
    Deps.laminar,
    Deps.waypoint,
    Deps.scribe.root
  )
}

object shared extends ScalaJSModule {
  override def scalaVersion = Deps.scalaVersion
  override def scalaJSVersion = Deps.scalaJSVersion
  override def ivyDeps = Agg(
    Deps.circe.core,
    Deps.refined,
    Deps.circe.refined,
    Deps.circe.generic,
    Deps.scalatags
  )
}
