package timetableplanner
package solver

import scala.annotation.tailrec
import scala.jdk.CollectionConverters.given
import java.util.List
import java.math.BigDecimal
import eu.timepit.refined.auto.given
import org.optaplanner.core.api.score
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore
import org.optaplanner.core.api.score.stream.{
  ConstraintProvider,
  ConstraintFactory
}
import org.optaplanner.core.api.score.stream.Joiners.{
  equal,
  filtering,
  overlapping
}
import org.optaplanner.core.api.score.stream.ConstraintCollectors.{
  min,
  sumBigDecimal,
  toList
}
import entities.Entity.{
  Teacher,
  ExcludedTimeIntervalConstraint,
  GapConstraint,
  RequiredSubjectGroupConstraint,
  TeacherEntryCountRangeConstraint
}

private class Constraints extends ConstraintProvider:
  private val overlappingWeek = filtering((e1: Entity, e2: Entity) =>
    e1.weekFrequency % e2.weekFrequency != 0
      && e2.weekFrequency % e1.weekFrequency != 0
      || e1.startingFromWeek == e2.startingFromWeek
  )

  private val overlappingTime = overlapping(
    (e: Entity) => e.minute,
    (e: Entity) => e.minute + e.length
  )

  @tailrec
  private def gcd(a: Int, b: Int): Int = if b == 0 then a else gcd(b, a % b)

  override def defineConstraints(factory: ConstraintFactory) =
    Array(
      factory
        .forEachUniquePair(
          classOf[Entity],
          equal((e: Entity) => e.`class`),
          overlappingTime,
          overlappingWeek,
          filtering((e1: Entity, e2: Entity) =>
            (e1.required, e2.required) match
              case (
                    r1: RequiredSubjectGroupConstraint,
                    r2: RequiredSubjectGroupConstraint
                  ) if r1 == r2 =>
                false
              case _ => true
          )
        )
        .penalize(
          "Class has more than one entry at the same time",
          HardSoftScore.ONE_HARD
        ),
      factory
        .forEachUniquePair(
          classOf[Entity],
          equal((e: Entity) => e.`class`),
          filtering((e1: Entity, e2: Entity) =>
            (e1.required, e2.required) match
              case (
                    r1: RequiredSubjectGroupConstraint,
                    r2: RequiredSubjectGroupConstraint
                  )
                  if r1 == r2 && r1.mustHappenAtTheSameTime
                    && e1.minute != e2.minute =>
                true
              case _ => false
          )
        )
        .penalize(
          """Classes in the same simultaneous subject group happen at different
            |times""".stripMargin,
          HardSoftScore.ONE_HARD
        ),
      factory
        .forEachUniquePair(
          classOf[Entity],
          equal((e: Entity) => e.`teacher`),
          overlappingTime,
          overlappingWeek
        )
        .penalize(
          "Teacher has more than one entry at the same time",
          HardSoftScore.ONE_HARD
        ),
      factory
        .forEachUniquePair(
          classOf[Entity],
          equal((e: Entity) => e.classroom),
          overlappingTime,
          overlappingWeek,
          filtering((e1: Entity, e2: Entity) =>
            if e1.classroom.isDefined then
              (e1.required, e2.required) match
                case (
                      r1: RequiredSubjectGroupConstraint,
                      r2: RequiredSubjectGroupConstraint
                    ) if r1 == r2 && r1.canHappenInTheSameClassroom =>
                  false
                case _ => true
            else false
          )
        )
        .penalize(
          "Classroom has more than one entry at the same time",
          HardSoftScore.ONE_HARD
        ),
      factory
        .forEachUniquePair(
          classOf[Entity],
          filtering((e1: Entity, e2: Entity) =>
            e1.`class` == e2.`class` && e1.subject == e2.subject
              && e1.teacher != e2.teacher
          )
        )
        .penalize(
          "Classroom taught same subject by multiple teachers",
          HardSoftScore.ONE_HARD
        ),
      factory
        .from(classOf[ExcludedTimeIntervalConstraint])
        .join(
          classOf[Entity],
          overlapping[ExcludedTimeIntervalConstraint, Entity, Integer](
            (i: ExcludedTimeIntervalConstraint) =>
              i.day * 60 * 24 + i.interval._1.hour * 60 + i.interval._1.minute,
            (i: ExcludedTimeIntervalConstraint) =>
              i.day * 60 * 24 + i.interval._2.hour * 60 + i.interval._2.minute,
            (e: Entity) => e.minute,
            (e: Entity) => e.minute + e.length
          ),
          filtering((i: ExcludedTimeIntervalConstraint, e: Entity) =>
            (i.classes contains e.`class`._1)
              || (e.classroom.isDefined
                && (i.classrooms contains e.classroom.get))
              || (i.subjects contains e.subject)
              || (i.teachers contains e.teacher)
          )
        )
        .penalize(
          "Entry in excluded time interval",
          HardSoftScore.ONE_SOFT,
          (i: ExcludedTimeIntervalConstraint, e: Entity) => i.priority: Int
        ),
      factory
        .from(classOf[GapConstraint])
        .join(
          classOf[Entity],
          filtering((m: GapConstraint, e: Entity) =>
            m.teachers contains e.teacher
          )
        )
        .join(
          classOf[Entity],
          filtering((m: GapConstraint, e1: Entity, e2: Entity) =>
            e1.teacher == e2.teacher && e2.minute >= e1.minute + e1.length
              && e1.minute / (24 * 60) == e2.minute / (24 * 60)
          )
        )
        .groupBy(
          (m: GapConstraint, e1: Entity, e2: Entity) => (m, e1),
          toList((m: GapConstraint, e1: Entity, e2: Entity) => e2)
        )
        .filter((t: (GapConstraint, Entity), nextJava: List[Entity]) =>
          val next = nextJava.asScala.filter(entity =>
            val dif = entity.minute - (t._2.minute + t._2.length)
            t._1.rangeMinutes._1 <= dif && dif <= t._1.rangeMinutes._2
          )
          if next.isEmpty then true
          else
            val gcdWeek =
              next.foldLeft(1)((acc, n) => gcd(acc, n.weekFrequency))
            (1 to gcdWeek).exists(week =>
              next.forall(next =>
                (week - 1) % next.weekFrequency != next.startingFromWeek - 1
              )
            )
        )
        .penalize(
          "Maximum entry gap for teacher exceeded",
          HardSoftScore.ONE_SOFT,
          (t: (GapConstraint, Entity), _: List[Entity]) => t._1.priority: Int
        ),
      factory
        .from(classOf[GapConstraint])
        .join(
          classOf[Entity],
          filtering((m: GapConstraint, e: Entity) =>
            m.classes contains e.`class`._1
          )
        )
        .join(
          classOf[Entity],
          filtering((m: GapConstraint, e1: Entity, e2: Entity) =>
            e1.`class` == e2.`class` && e2.minute >= e1.minute + e1.length
              && e1.minute / (24 * 60) == e2.minute / (24 * 60)
          )
        )
        .groupBy(
          (m: GapConstraint, e1: Entity, e2: Entity) => (m, e1),
          toList((m: GapConstraint, e1: Entity, e2: Entity) => e2)
        )
        .filter((t: (GapConstraint, Entity), nextJava: List[Entity]) =>
          val next = nextJava.asScala.filter(entity =>
            val dif = entity.minute - (t._2.minute + t._2.length)
            t._1.rangeMinutes._1 <= dif && dif <= t._1.rangeMinutes._2
          )
          if next.isEmpty then true
          else
            val gcdWeek =
              next.foldLeft(1)((acc, n) => gcd(acc, n.weekFrequency))
            val grouped = next.groupBy(next =>
              next.required match
                case r: RequiredSubjectGroupConstraint => Some(r)
                case _                                 => None
            )
            (1 to gcdWeek).exists(week =>
              grouped.values.exists(group =>
                group.forall(next =>
                  (week - 1) % next.weekFrequency != next.startingFromWeek - 1
                )
              )
            )
        )
        .penalize(
          "Maximum entry gap for class exceeded",
          HardSoftScore.ONE_SOFT,
          (t: (GapConstraint, Entity), _: List[Entity]) => t._1.priority: Int
        ),
      factory
        .from(classOf[GapConstraint])
        .join(
          classOf[Entity],
          filtering((m: GapConstraint, e: Entity) =>
            m.subjects contains e.subject
          )
        )
        .join(
          classOf[Entity],
          filtering((m: GapConstraint, e1: Entity, e2: Entity) =>
            e1.`class` == e2.`class` && e1.subject == e2.subject
              && e2.minute >= e1.minute + e1.length
              && e1.minute / (24 * 60) == e2.minute / (24 * 60)
          )
        )
        .groupBy(
          (m: GapConstraint, e1: Entity, e2: Entity) => (m, e1),
          toList((m: GapConstraint, e1: Entity, e2: Entity) => e2)
        )
        .filter((t: (GapConstraint, Entity), nextJava: List[Entity]) =>
          val next = nextJava.asScala.filter(entity =>
            val dif = entity.minute - (t._2.minute + t._2.length)
            t._1.rangeMinutes._1 <= dif && dif <= t._1.rangeMinutes._2
          )
          if next.isEmpty then true
          else
            val gcdWeek =
              next.foldLeft(1)((acc, n) => gcd(acc, n.weekFrequency))
            val grouped = next.groupBy(next =>
              next.required match
                case r: RequiredSubjectGroupConstraint => Some(r)
                case _                                 => None
            )
            (1 to gcdWeek).exists(week =>
              grouped.values.exists(group =>
                group.forall(next =>
                  (week - 1) % next.weekFrequency != next.startingFromWeek - 1
                )
              )
            )
        )
        .penalize(
          "Maximum entry gap for a subject for a class exceeded",
          HardSoftScore.ONE_SOFT,
          (t: (GapConstraint, Entity), _: List[Entity]) => t._1.priority: Int
        ),
      factory
        .from(classOf[TeacherEntryCountRangeConstraint])
        .join(
          classOf[Entity],
          filtering((r: TeacherEntryCountRangeConstraint, e: Entity) =>
            r.teachers contains e.teacher
          )
        )
        .groupBy(
          (r: TeacherEntryCountRangeConstraint, e: Entity) => (r, e.teacher),
          sumBigDecimal((r: TeacherEntryCountRangeConstraint, e: Entity) =>
            BigDecimal(1) divide BigDecimal(e.weekFrequency)
          )
        )
        .filter(
          (t: (TeacherEntryCountRangeConstraint, Teacher), count: BigDecimal) =>
            (count compareTo BigDecimal(
              t._1.range._1
            )) < 0 || (count compareTo BigDecimal(t._1.range._2)) < 0
        )
        .penalize(
          "Teacher not given the required number of entries",
          HardSoftScore.ONE_SOFT,
          (t: (TeacherEntryCountRangeConstraint, Teacher), count: BigDecimal) =>
            t._1.priority: Int
        ),
      factory
        .from(classOf[TeacherEntryCountRangeConstraint])
        .join(
          classOf[Teacher],
          filtering((r: TeacherEntryCountRangeConstraint, t: Teacher) =>
            r.teachers contains t
          )
        )
        .ifNotExists(
          classOf[Entity],
          filtering(
            (r: TeacherEntryCountRangeConstraint, t: Teacher, e: Entity) =>
              t == e.teacher
          )
        )
        .penalize(
          "Teacher given no entries despite having a required number of entries",
          HardSoftScore.ONE_SOFT,
          (r: TeacherEntryCountRangeConstraint, t: Teacher) => r.priority: Int
        ),
      factory
        .fromUniquePair(
          classOf[Entity],
          equal((e: Entity) => e.`class`),
          equal((e: Entity) => e.required),
          equal((e: Entity) => e.startingFromWeek),
          filtering((e1: Entity, e2: Entity) => e1.subject != e2.subject),
          filtering((e1: Entity, e2: Entity) =>
            e1.required match
              case r: RequiredSubjectGroupConstraint if r.spansMultipleWeeks =>
                true
              case _ => false
          )
        )
        .penalize(
          "Subject group spanning multiple weeks happens the same week",
          HardSoftScore.ONE_HARD
        )
    )
