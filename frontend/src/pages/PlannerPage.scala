package timetableplanner
package pages

import scala.collection.immutable.TreeSet
import scala.annotation.targetName
import scala.scalajs.js.Dictionary
import org.scalajs.dom.HttpMethod.POST
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.Refined
import com.raquo.laminar.api.L.{*, given}
import com.raquo.domtypes.generic.codecs.StringAsIsCodec
import io.circe.refined.given
import events.{Event, Name, GlobalPatch}
import entities.{
  Timetable,
  TimetableState,
  Entity,
  LocaleFormatter,
  DayOfTheWeek,
  TimeOfDay
}
import components.{entityList, dialog, ValidatedInput, given}

private val labelAttr = customHtmlAttr("label", StringAsIsCodec)

object JsLocaleFormatter extends LocaleFormatter:
  def format(number: Int) =
    Intl
      .NumberFormat(documentLang, Dictionary.empty)
      .format(number)

  def format(time: TimeOfDay) =
    timeOfDayInput.format(time)

  @targetName("formatDay")
  def format(day: Int Refined DayOfTheWeek) =
    weekDayInput.format(day)

def plannerPageView(using planner: Planner)(using
    messagesSignal: Signal[Map[String, String]]
) =
  main(
    child.maybe <-- planner.schedules.map(_.isEmpty).map {
      case true => None
      case false =>
        val selectSchedule = select(
          children <-- planner.schedules
            .map(_.keys.toSeq.sortBy(n => n: String))
            .split(identity)((name, _, _) => option(name: String))
        )
        selectSchedule.amend(
          composeEvents(onInput)(_.sample(planner.schedules)) --> (schedules =>
            planner.currentSchedule set Some(
              (schedules.keys.toSeq
                .sortBy(n => n: String): Seq[String Refined Name])(
                selectSchedule.ref.selectedIndex
              )
            )
          )
        )
        Some(label(child.text <-- messages("schedule"), selectSchedule))
    }, {
      val name = ValidatedInput[Name]("", "nameRequirements")
      val modal = dialog(
        label(child.text <-- messages("name"), name),
        dialog.close.mapTo(()) --> name.clear
      )
      modal.amend(
        button(
          typ := "button",
          child.text <-- messages("close"),
          onClick --> (_ => modal.ref.close(""))
        ),
        button(
          typ := "button",
          child.text <-- messages("add"),
          disabled <-- name.validated.map(_.isEmpty),
          composeEvents(onClick)(
            _.sample(name.validated)
          ) --> (name =>
            modal.ref.close("")
            planner.patchSchedules onNext GlobalPatch.Add(name.get)
          )
        )
      )
      val addButton = button(
        typ := "button",
        child.text <-- messages("addSchedule"),
        onClick --> (_ => modal.ref.showModal())
      )
      Vector(addButton, modal)
    },
    child.maybe <-- planner.currentSchedule.signal.map {
      case None => None
      case Some(name) =>
        Some(
          div(
            button(
              typ := "button",
              child.text <-- messages("removeSchedule"),
              onClick.mapTo(GlobalPatch.Remove(name)) --> planner.patchSchedules
            ),
            entityList[Entity.Subject],
            entityList[Entity.Teacher],
            entityList[Entity.Class],
            entityList[Entity.Classroom],
            entityList[Entity.AlignmentConstraint],
            entityList[Entity.RequiredLengthConstraint],
            entityList[Entity.RequiredSubjectConstraint],
            entityList[Entity.RequiredSubjectGroupConstraint],
            entityList[Entity.RequiredClassroomConstraint],
            entityList[Entity.ExcludedTimeIntervalConstraint],
            entityList[Entity.GapConstraint],
            entityList[Entity.TeacherEntryCountRangeConstraint],
            button(
              typ := "button",
              child.text <-- messages("solve"),
              composeEvents(onClick)(
                _.flatMap(_ =>
                  fetch(POST, "/api/solve", planner.currentSchedule.now().get)
                )
              ) --> (response =>
                if !response.ok then
                  scribe.error(s"Solve returned ${response.status}")
              )
            ),
            child <-- planner.timetable.combineWithFn(messagesSignal) {
              case (TimetableState.Solving, messages) =>
                div(
                  div(cls := "spinner", div(), div(), div()),
                  p(messages("solving"))
                )
              case (TimetableState.None, messages) => p(messages("noTimetable"))
              case (TimetableState.Solved(timetable, _), messages) =>
                val (subjectsSet, classesSet, teachersSet, classroomsSet) =
                  timetable.foldLeft(
                    (
                      TreeSet.empty[String],
                      TreeSet.empty[String],
                      TreeSet.empty[String],
                      TreeSet.empty[String]
                    )
                  ) { case ((subjects, classes, teachers, classrooms), entry) =>
                    (
                      subjects + entry.subject,
                      classes + entry.`class`,
                      teachers + entry.teacher,
                      entry.classroom match
                        case None            => classrooms
                        case Some(classroom) => classrooms + classroom
                    )
                  }
                val (subjects, classes, teachers, classrooms) = (
                  subjectsSet.toSeq,
                  classesSet.toSeq,
                  teachersSet.toSeq,
                  classroomsSet.toSeq
                )
                def group(collection: Seq[String], label: String) =
                  optGroup(
                    labelAttr := messages(label),
                    collection.toVector.sorted.map(option(_))
                  )
                val selectFilter = select(
                  option(messages("all")),
                  group(subjects, "subjects"),
                  group(classes, "classes"),
                  group(teachers, "teachers"),
                  group(classrooms, "classrooms")
                )
                val result = div(
                  label(
                    messages("published"),
                    input(
                      typ := "checkbox",
                      composeEvents(onClick.mapToChecked.map((name, _)))(
                        _.flatMap(body => fetch(POST, "/api/publish", body))
                      ) --> (response =>
                        if !response.ok then
                          scribe.error(
                            s"Publish returned ${response.status}"
                          )
                      ),
                      checked <-- planner.published
                    )
                  ),
                  selectFilter
                )
                selectFilter.amend(
                  onChange --> (_ =>
                    val index = selectFilter.ref.selectedIndex
                    result.ref.removeChild(result.ref.lastChild)
                    val filter =
                      if index == 0 then None
                      else
                        val indexNoAll = index - 1
                        if indexNoAll < subjects.size then
                          Some(
                            Timetable.ShowFilter.Subject,
                            subjects(indexNoAll)
                          )
                        else
                          val indexNoSubjects = indexNoAll - subjects.size
                          if indexNoSubjects < classes.size then
                            Some(
                              Timetable.ShowFilter.Class,
                              classes(indexNoSubjects)
                            )
                          else
                            val indexNoClasses =
                              indexNoSubjects - classes.size
                            if indexNoClasses < teachers.size then
                              Some(
                                Timetable.ShowFilter.Teacher,
                                teachers(indexNoClasses)
                              )
                            else
                              Some(
                                Timetable.ShowFilter.Classroom,
                                classrooms(indexNoClasses - teachers.size)
                              )
                    result.ref.appendChild(
                      Timetable
                        .show(
                          scalatags.JsDom,
                          JsLocaleFormatter,
                          messages,
                          timetable,
                          filter
                        )
                        .render
                    )
                  )
                )
                result.ref.appendChild(
                  Timetable
                    .show(
                      scalatags.JsDom,
                      JsLocaleFormatter,
                      messages,
                      timetable
                    )
                    .render
                )
                result
            }
          )
        )
    }
  )
