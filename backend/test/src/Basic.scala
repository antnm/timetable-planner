package timetableplanner
package tests

import munit.FunSuite
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto.given
import solver.Solver
import entities.{Schedule, Entity, Timetable, TimeOfDay}
import Entity.*
import Timetable.*

given [A, R]: Conversion[A, A Refined R] = Refined.unsafeApply

class Basic extends FunSuite:
  val solver = Solver()

  test("empty") {
    assertEquals(Timetable(), solver.solve(Schedule()))
  }

  test("one-entry") {
    val subject: Subject = Subject(name = "s")
    val teacher = Teacher(name = "t", Vector(subject))
    val `class`: Class = Class(name = "c")
    val required =
      RequiredSubjectConstraint(count = 1, Vector(subject), Vector(`class`))
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(9, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    assertEquals(
      solver.solve(Schedule(subject, teacher, `class`, required, alignment)),
      Set(
        Timetable.Entry(
          `class` = "c",
          teacher = "t",
          subject = "s",
          start = TimeOfDay(8, 0),
          end = TimeOfDay(9, 0),
          day = 0
        )
      )
    )
  }

  test("one-entry-with-classroom") {
    val subject: Subject = Subject(name = "s")
    val teacher = Teacher(name = "t", Vector(subject))
    val `class`: Entity.Class = Class(name = "c")
    val classroom: Entity.Classroom = Classroom(name = "cr")
    val requiredSubject =
      RequiredSubjectConstraint(subjects = Vector(subject), Vector(`class`))
    val requiredClassroom =
      RequiredClassroomConstraint(subjects = Vector(subject), Vector(classroom))
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(9, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    assertEquals(
      solver.solve(
        Schedule(
          subject,
          teacher,
          `class`,
          requiredSubject,
          requiredClassroom,
          alignment
        )
      ),
      Set(
        Timetable.Entry(
          `class` = "c",
          teacher = "t",
          subject = "s",
          start = TimeOfDay(8, 0),
          end = TimeOfDay(9, 0),
          day = 0,
          classroom = Some("cr")
        )
      )
    )
  }

  test("two-classes-one-classroom") {
    val subject1: Subject = Subject(name = "s1")
    val teacher1 = Teacher(name = "t1", Vector(subject1))
    val class1: Class = Class(name = "c1", 1)
    val requiredSubject1 =
      RequiredSubjectConstraint(subjects = Vector(subject1), Vector(class1))
    val subject2: Subject = Subject(name = "s2")
    val teacher2 = Teacher(name = "t2", Vector(subject2))
    val class2: Class = Class(name = "c2", 1)
    val requiredSubject2 =
      RequiredSubjectConstraint(subjects = Vector(subject2), Vector(class2))
    val classroom: Classroom = Classroom(name = "cr")
    val requiredClassroom = RequiredClassroomConstraint(
      subjects = Vector(subject1, subject2),
      Vector(classroom)
    )
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(10, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject1, subject2)
    )
    assertEquals(
      solver.solve(
        Schedule(
          subject1,
          teacher1,
          class1,
          requiredSubject1,
          subject2,
          teacher2,
          class2,
          requiredSubject2,
          classroom,
          requiredClassroom,
          alignment
        )
      ),
      Set(
        Timetable.Entry(
          `class` = "c1",
          teacher = "t1",
          subject = "s1",
          start = TimeOfDay(8, 0),
          end = TimeOfDay(9, 0),
          day = 0,
          classroom = Some("cr")
        ),
        Timetable.Entry(
          `class` = "c2",
          teacher = "t2",
          subject = "s2",
          start = TimeOfDay(9, 0),
          end = TimeOfDay(10, 0),
          day = 0,
          classroom = Some("cr")
        )
      )
    )
  }

  test("one-teacher-one-class-two-entries") {
    val subject: Subject = Subject(name = "s")
    val teacher = Teacher(name = "t", Vector(subject))
    val `class`: Class = Class(name = "c")
    val required =
      RequiredSubjectConstraint(count = 2, Vector(subject), Vector(`class`))
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(10, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    assertEquals(
      solver.solve(Schedule(subject, teacher, `class`, required, alignment)),
      Set(
        Timetable.Entry(
          `class` = "c",
          teacher = "t",
          subject = "s",
          start = TimeOfDay(8, 0),
          end = TimeOfDay(9, 0),
          day = 0
        ),
        Timetable.Entry(
          `class` = "c",
          teacher = "t",
          subject = "s",
          start = TimeOfDay(9, 0),
          end = TimeOfDay(10, 0),
          day = 0
        )
      )
    )
  }

  test("one-teacher-two-classes") {
    val subject: Subject = Subject(name = "s")
    val teacher = Teacher(name = "t", Vector(subject))
    val `class`: Class = Class(name = "c", 2)
    val required =
      RequiredSubjectConstraint(subjects = Vector(subject), Vector(`class`))
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(10, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    assertEquals(
      solver.solve(Schedule(subject, teacher, `class`, required, alignment)),
      Set(
        Timetable.Entry(
          `class` = "c1",
          teacher = "t",
          subject = "s",
          start = TimeOfDay(8, 0),
          end = TimeOfDay(9, 0),
          day = 0
        ),
        Timetable.Entry(
          `class` = "c2",
          teacher = "t",
          subject = "s",
          start = TimeOfDay(9, 0),
          end = TimeOfDay(10, 0),
          day = 0
        )
      )
    )
  }

  test("one-subject-group") {
    val subject1: Subject = Subject(name = "s1")
    val teacher1 = Teacher(name = "t1", Vector(subject1))
    val subject2: Entity.Subject = Subject(name = "s2")
    val teacher2 = Teacher(name = "t2", Vector(subject2))
    val `class`: Entity.Class = Class(name = "c", 1)
    val required =
      RequiredSubjectGroupConstraint(
        subjects = Vector(subject1, subject2),
        Vector(`class`),
        canHappenInTheSameClassroom = true,
        mustHappenAtTheSameTime = true,
        spansMultipleWeeks = false
      )
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(10, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject1, subject2)
    )
    assertEquals(
      solver.solve(
        Schedule(
          subject1,
          teacher1,
          subject2,
          teacher2,
          `class`,
          required,
          alignment
        )
      ),
      Set(
        Timetable.Entry(
          `class` = "c",
          teacher = "t1",
          subject = "s1",
          start = TimeOfDay(8, 0),
          end = TimeOfDay(9, 0),
          day = 0
        ),
        Timetable.Entry(
          `class` = "c",
          teacher = "t2",
          subject = "s2",
          start = TimeOfDay(8, 0),
          end = TimeOfDay(9, 0),
          day = 0
        )
      )
    )
  }

  test("one-subject-group-with-excluded-subject-time") {
    val subject1: Entity.Subject = Subject(name = "s1")
    val teacher1 = Teacher(name = "t1", Vector(subject1))
    val subject2: Entity.Subject = Subject(name = "s2")
    val teacher2 = Teacher(name = "t2", Vector(subject2))
    val `class`: Entity.Class = Class(name = "c")
    val required =
      RequiredSubjectGroupConstraint(
        subjects = Vector(subject1, subject2),
        Vector(`class`),
        canHappenInTheSameClassroom = true,
        mustHappenAtTheSameTime = true,
        spansMultipleWeeks = false
      )
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(10, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject1, subject2)
    )
    val excluded = ExcludedTimeIntervalConstraint(
      priority = 1,
      day = 0,
      (TimeOfDay(8, 0), TimeOfDay(9, 0)),
      Vector(subject1)
    )
    assertEquals(
      solver.solve(
        Schedule(
          subject1,
          teacher1,
          subject2,
          teacher2,
          `class`,
          required,
          alignment,
          excluded
        )
      ),
      Set(
        Timetable.Entry(
          `class` = "c",
          teacher = "t1",
          subject = "s1",
          start = TimeOfDay(9, 0),
          end = TimeOfDay(10, 0),
          day = 0
        ),
        Timetable.Entry(
          `class` = "c",
          teacher = "t2",
          subject = "s2",
          start = TimeOfDay(9, 0),
          end = TimeOfDay(10, 0),
          day = 0
        )
      )
    )
  }

  test("two-entries-with-no-gap-for-teacher") {
    val subject: Entity.Subject = Subject(name = "s")
    val teacher: Teacher = Teacher(name = "t", Vector(subject))
    val `class`: Entity.Class = Class(name = "c")
    val required =
      RequiredSubjectConstraint(count = 2, Vector(subject), Vector(`class`))
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(1, 0), TimeOfDay(23, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    val gap = GapConstraint(
      priority = 1,
      (Refined.unsafeApply(0), Refined.unsafeApply(0)),
      Vector(teacher)
    )
    val solution = solver
      .solve(Schedule(subject, teacher, `class`, required, alignment, gap))
      .toVector
    solution foreach (e =>
      assertEquals(e.`class`: String, "c")
      assertEquals(e.teacher: String, "t")
      assertEquals(e.subject: String, "s")
      assertEquals(e.day: Int, 0)
      assertEquals(e.classroom, None)
      assertEquals(e.end.asMinutes - e.start.asMinutes, 60)
    )
    assertEquals(solution.size, 2)
    assertEquals(
      (solution(0).start.asMinutes - solution(1).start.asMinutes).abs,
      60
    )
  }

  test("two-entries-with-no-gap-for-class") {
    val subject: Entity.Subject = Subject(name = "s")
    val teacher: Teacher = Teacher(name = "t", Vector(subject))
    val `class`: Entity.Class = Class(name = "c")
    val required =
      RequiredSubjectConstraint(count = 2, Vector(subject), Vector(`class`))
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(1, 0), TimeOfDay(23, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    val gap = GapConstraint(
      priority = 1,
      (Refined.unsafeApply(0), Refined.unsafeApply(0)),
      classes = Vector(`class`)
    )
    val solution = solver
      .solve(Schedule(subject, teacher, `class`, required, alignment, gap))
      .toVector
    solution foreach (e =>
      assertEquals(e.`class`: String, "c")
      assertEquals(e.teacher: String, "t")
      assertEquals(e.subject: String, "s")
      assertEquals(e.day: Int, 0)
      assertEquals(e.classroom, None)
      assertEquals(e.end.asMinutes - e.start.asMinutes, 60)
    )
    assertEquals(solution.size, 2)
    assertEquals(
      (solution(0).start.asMinutes - solution(1).start.asMinutes).abs,
      60
    )
  }

  test("two-teachers-with-two-entry-counts") {
    val subject: Subject = Subject(name = "s")
    val teacher1: Teacher = Teacher(name = "t1", Vector(subject))
    val teacher2: Teacher = Teacher(name = "t2", Vector(subject))
    val `class`: Class = Class(name = "c", 10)
    val requiredSubject =
      RequiredSubjectConstraint(count = 1, Vector(subject), Vector(`class`))
    val teacherEntryCountRange = TeacherEntryCountRangeConstraint(
      priority = 1,
      (Refined.unsafeApply(5), Refined.unsafeApply(5)),
      Vector(teacher1, teacher2)
    )
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(18, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    val solution = solver.solve(
      Schedule(
        subject,
        teacher1,
        teacher2,
        `class`,
        requiredSubject,
        teacherEntryCountRange,
        alignment
      )
    )
    assertEquals(solution.count(e => (e.teacher: String) == "t1"), 5)
    assertEquals(solution.count(e => (e.teacher: String) == "t2"), 5)
  }

  test("two-teachers-with-one-entry-count") {
    val subject: Subject = Subject(name = "s")
    val teacher1: Teacher = Teacher(name = "t1", Vector(subject))
    val teacher2: Teacher = Teacher(name = "t2", Vector(subject))
    val `class`: Class = Class(name = "c", 10)
    val requiredSubject =
      RequiredSubjectConstraint(count = 1, Vector(subject), Vector(`class`))
    val teacherEntryCountRange = TeacherEntryCountRangeConstraint(
      priority = 1,
      (Refined.unsafeApply(5), Refined.unsafeApply(5)),
      Vector(teacher1)
    )
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(18, 0)),
      step = 60,
      onTheDaysOf = Set(0),
      subjects = Vector(subject)
    )
    val solution = solver.solve(
      Schedule(
        subject,
        teacher1,
        teacher2,
        `class`,
        requiredSubject,
        teacherEntryCountRange,
        alignment
      )
    )
    assertEquals(solution.count(e => (e.teacher: String) == "t1"), 5)
    assertEquals(solution.count(e => (e.teacher: String) == "t2"), 5)
  }

  test("subject-week-frequency") {
    val subject: Subject = Subject(name = "s")
    val teacher: Teacher = Teacher(name = "t", Vector(subject))
    val `class`: Class = Class(name = "c", 1)
    val requiredSubject =
      RequiredSubjectConstraint(
        weekFrequency = 2,
        1,
        Vector(subject),
        Vector(`class`)
      )
    val solution =
      solver.solve(Schedule(subject, teacher, `class`, requiredSubject))
    assertEquals(solution.size, 1)
    assertEquals(solution.head.weekFrequency: Int, 2)
  }

  test("two-subjects-with-week-frequency-same-interval") {
    val subject1: Subject = Subject(name = "s1")
    val subject2: Subject = Subject(name = "s2")
    val teacher: Teacher = Teacher(name = "t", Vector(subject1, subject2))
    val `class`: Class = Class(name = "c", 1)
    val requiredSubject1 =
      RequiredSubjectConstraint(
        weekFrequency = 2,
        1,
        Vector(subject1),
        Vector(`class`)
      )
    val requiredSubject2 =
      RequiredSubjectConstraint(
        weekFrequency = 2,
        1,
        Vector(subject2),
        Vector(`class`)
      )
    val alignment = AlignmentConstraint(
      interval = (TimeOfDay(8, 0), TimeOfDay(9, 0)),
      60,
      Set(0),
      Vector(subject1, subject2)
    )
    val solution =
      solver.solve(
        Schedule(
          subject1,
          subject2,
          teacher,
          `class`,
          requiredSubject1,
          requiredSubject2,
          alignment
        )
      )
    assertEquals(solution.size, 2)
    solution.foreach(e =>
      assertEquals(e.teacher: String, "t")
      assertEquals(e.`class`: String, "c")
      assertEquals(e.weekFrequency: Int, 2)
      assertEquals(e.start, TimeOfDay(8, 0))
      assertEquals(e.end, TimeOfDay(9, 0))
      assert(e.startingFromWeek >= 1)
      assert(e.startingFromWeek <= 2)
    )
    assert(solution.exists(e => (e.subject: String) == "s1"))
    assert(solution.exists(e => (e.subject: String) == "s2"))
    assertNotEquals(
      solution.head.startingFromWeek,
      solution.tail.head.startingFromWeek
    )
  }

  test("subject-group-week-frequency") {
    val subject1: Subject = Subject(name = "s1")
    val subject2: Subject = Subject(name = "s2")
    val teacher1: Teacher = Teacher(name = "t1", Vector(subject1))
    val teacher2: Teacher = Teacher(name = "t2", Vector(subject2))
    val `class`: Class = Class(name = "c", 1)
    val requiredSubjectGroup =
      RequiredSubjectGroupConstraint(
        count = 1,
        Vector(subject1, subject2),
        Vector(`class`),
        canHappenInTheSameClassroom = true,
        spansMultipleWeeks = true,
        mustHappenAtTheSameTime = false
      )
    val solution =
      solver.solve(
        Schedule(
          subject1,
          subject2,
          teacher1,
          teacher2,
          `class`,
          requiredSubjectGroup
        )
      )
    assertEquals(solution.size, 2)
    assert(
      solution.exists(e =>
        (e.teacher: String) == "t1" && (e.`class`: String) == "c"
          && (e.subject: String) == "s1"
      )
    )
    assert(
      solution.exists(e =>
        (e.teacher: String) == "t2" && (e.`class`: String) == "c"
          && (e.subject: String) == "s2"
      )
    )
    solution.foreach(e =>
      assertEquals(e.weekFrequency: Int, 2)
      assert(e.startingFromWeek >= 1)
      assert(e.startingFromWeek <= 2)
    )
    assertNotEquals(
      solution.head.startingFromWeek,
      solution.tail.head.startingFromWeek
    )
  }

  test("subject-group-week-frequency-with-count") {
    val subject1: Subject = Subject(name = "s1")
    val subject2: Subject = Subject(name = "s2")
    val teacher1: Teacher = Teacher(name = "t1", Vector(subject1))
    val teacher2: Teacher = Teacher(name = "t2", Vector(subject2))
    val `class`: Class = Class(name = "c", 1)
    val requiredSubjectGroup =
      RequiredSubjectGroupConstraint(
        count = 2,
        Vector(subject1, subject2),
        Vector(`class`),
        canHappenInTheSameClassroom = true,
        spansMultipleWeeks = true,
        mustHappenAtTheSameTime = false
      )
    val solution =
      solver.solve(
        Schedule(
          subject1,
          subject2,
          teacher1,
          teacher2,
          `class`,
          requiredSubjectGroup
        )
      )
    assertEquals(solution.size, 4)
    assertEquals(
      solution.count(e =>
        (e.teacher: String) == "t1" && (e.`class`: String) == "c"
          && (e.subject: String) == "s1"
      ),
      2
    )
    assertEquals(
      solution.count(e =>
        (e.teacher: String) == "t2" && (e.`class`: String) == "c"
          && (e.subject: String) == "s2"
      ),
      2
    )
    solution.foreach(e =>
      assertEquals(e.weekFrequency: Int, 2)
      assert(e.startingFromWeek >= 1)
      assert(e.startingFromWeek <= 2)
    )
    assertEquals(
      solution
        .filter(e => (e.subject: String) == "s1")
        .map(_.startingFromWeek)
        .toSet
        .size,
      1
    )
    assertEquals(
      solution
        .filter(e => (e.subject: String) == "s2")
        .map(_.startingFromWeek)
        .toSet
        .size,
      1
    )
    assertNotEquals(
      solution
        .filter(e => (e.subject: String) == "s1")
        .head
        .startingFromWeek,
      solution
        .filter(e => (e.subject: String) == "s2")
        .head
        .startingFromWeek
    )
  }
