create table users (
    id       int  primary key generated always as identity,
    name     text unique not null,
    password text not null
);

insert into users(name, password)
values ('admin', '$s0$e0801$xtLkauEG1KXitpsY2repqw==$vQIsotk1s36UXWxzmkNHkcfNOEZUy2r3vxRSZ9A//3Q=');
