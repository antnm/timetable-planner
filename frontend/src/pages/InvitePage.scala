package timetableplanner
package pages

import com.raquo.laminar.api.L.{*, given}
import org.scalajs.dom.HttpMethod.PUT

def invitePageView(using Signal[Map[String, String]]) =
  val inviteBus = EventBus[String]()
  main(
    h1(child.text <-- messages("sendAnInvitation")),
    button(
      typ := "button",
      child.text <-- messages("generateInvitation"),
      composeEvents(onClick)(_.flatMap(_ =>
        scribe.debug("Generating invite")
        fetch(PUT, "/api/invite").flatMap(_.data[String])
      )) --> inviteBus.writer
    ),
    child <-- inviteBus.events.map(code =>
      div(
        label(
          child.text <-- messages("invitationExplanation"),
          input(
            value := code,
            readOnly := true
          )
        )
      )
    )
  )
