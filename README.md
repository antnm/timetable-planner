Real-time collaborative webapp that solves the timetable problem using constraint programming.

To run, use the following command:

```
podman-compose up
```

![](screenshots/biology-timetable.png)

![](screenshots/login.png)

![](screenshots/teacher.png)

![](screenshots/subjects.png)

![](screenshots/class-timetable.png)
