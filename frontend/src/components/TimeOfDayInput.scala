package timetableplanner
package components

import scala.util.Try
import scala.scalajs.js.{Dictionary, Date}
import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.{Refined, Validate}
import entities.{Schedule, TimeOfDay}

val timeRegex = raw"(\d{2}):(\d{2})".r
case class ValidTimeOfDay()
given Validate.Plain[String, ValidTimeOfDay] =
  Validate.fromPredicate(
    str =>
      str match
        case timeRegex(hourStr, minuteStr) =>
          val hour = hourStr.toInt
          val minute = minuteStr.toInt
          0 <= hour && hour < 24 && 0 <= minute && minute < 60
        case _ => false
    ,
    str => s"$str is a valid time of day",
    ValidTimeOfDay()
  )

given timeOfDayInput: EntityComponent[TimeOfDay] with
  def format(t: TimeOfDay) =
    Intl
      .DateTimeFormat(documentLang, Dictionary("timeStyle" -> "short"))
      .format(
        new Date(
          year = 0,
          month = 0,
          date = 0,
          hours = t.hour,
          minutes = t.minute
        )
      )

  def show(uuid: UUID, s: Signal[Schedule], t: TimeOfDay)(using
      Signal[Map[String, String]]
  ) = Vector(
    span(format(t))
  )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val input =
      ValidatedInput[ValidTimeOfDay]("", "timeRequirement", typ := "time")
    EntityComponent.Editable(
      Seq(input),
      input.validated.map(
        _.map(v =>
          Refined.unsafeApply((v: String) match
            case timeRegex(hour, minute) =>
              TimeOfDay(
                Refined.unsafeApply(hour.toInt),
                Refined.unsafeApply(minute.toInt)
              )
          )
        )
      ),
      input.clear,
      Observer(t => input.value set "%02d:%02d".format(t.hour, t.minute))
    )
