package timetableplanner

import com.raquo.laminar.api.L.{*, given}
import com.raquo.waypoint.*
import pages.*

sealed trait Page

object Page:
  def parse(str: String) =
    Seq(LoginPage, RegisterPage, PlannerPage, UserPage, InvitePage).find(
      _.toString == str
    )

case object LoginPage extends Page

case object RegisterPage extends Page

sealed trait AuthenticatedPage extends Page

case object PlannerPage extends AuthenticatedPage

case object UserPage extends AuthenticatedPage

case object InvitePage extends AuthenticatedPage

def mkRouter(messages: StrictSignal[Map[String, String]]) = Router(
  routes = List(
    Route.static(LoginPage, root / "login" / endOfSegments),
    Route.static(RegisterPage, root / "register" / endOfSegments),
    Route.static(PlannerPage, root / "planner" / endOfSegments),
    Route.static(UserPage, root / "user" / endOfSegments),
    Route.static(InvitePage, root / "invites" / endOfSegments)
  ),
  serializePage = _.toString,
  deserializePage = Page.parse(_).get,
  getPageTitle = page =>
    val str = page.toString
    s"Timetable Planner - ${messages.now()(str.head.toLower +: str.tail)}"
)(
  $popStateEvent = windowEvents.onPopState,
  owner = unsafeWindowOwner
)

def mkSplitter(using router: Router[Page])(using
    Planner,
    Var[User],
    Signal[Map[String, String]]
) =
  SplitRender[Page, HtmlElement](router.$currentPage)
    .collectStatic(LoginPage)(loginPage)
    .collectStatic(RegisterPage)(registerPage)
    .collectSignal[AuthenticatedPage](
      authenticatedPage(_)
    )
