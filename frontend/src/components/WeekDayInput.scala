package timetableplanner
package components

import scala.util.Try
import scala.scalajs.js.{Array, Dictionary, Date}
import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.{Refined, Validate}
import entities.{Schedule, DayOfTheWeek}

given weekDayInput: EntityComponent[Int Refined DayOfTheWeek] with
  def format(day: Int Refined DayOfTheWeek) = Intl
    .DateTimeFormat(documentLang, Dictionary("weekday" -> "long"))
    .format(new Date(0, 0, 1 + day)) // 1900-01-01 was a Monday

  def show(
      uuid: UUID,
      s: Signal[Schedule],
      d: Int Refined DayOfTheWeek
  )(using Signal[Map[String, String]]) =
    Vector(span(format(d)))

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val current = Var[Int Refined DayOfTheWeek](Refined.unsafeApply(0))
    val s =
      select(
        (0 until 7)
          .map(Refined.unsafeApply[Int, DayOfTheWeek])
          .toVector
          .map(d => option(format(d)))
      )
    s.amend(
      onChange.mapTo(Refined.unsafeApply(s.ref.selectedIndex)) --> current,
      current.signal --> (i => s.ref.selectedIndex = i)
    )
    EntityComponent.Editable(
      Vector(s),
      current.signal.map(Some(_)),
      Observer(_ => current set Refined.unsafeApply(0)),
      current.toObserver
    )
