package timetableplanner
package entities

import eu.timepit.refined.api.Refined
import io.circe.{Encoder, Decoder}
import io.circe.generic.semiauto.{deriveEncoder, deriveDecoder}
import events.Name

enum TimetableState:
  case Solved(timetable: Timetable, published: Boolean)
  case Solving
  case None

case class StateEntry(schedule: Schedule, timetable: TimetableState)

given Encoder[StateEntry] = deriveEncoder
given Decoder[StateEntry] = deriveDecoder

type State = Map[String Refined Name, StateEntry]

object State:
  def apply(): State = Map.empty

extension (state: State)
  def schedules = state.map((k, v) => (k, v.schedule))
  def updateSchedules(schedules: Map[String Refined Name, Schedule]) =
    schedules.map((k, v) =>
      val prev = state.get(k)
      (
        k,
        StateEntry(
          v,
          prev match
            case Some(prev) => prev.timetable
            case _          => TimetableState.None
        )
      )
    )
