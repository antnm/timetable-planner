package timetableplanner
package events

import scala.deriving.Mirror
import scala.compiletime.{erasedValue, summonInline}
import java.util.UUID
import eu.timepit.refined.api.Refined
import io.circe.{Encoder, Decoder, HCursor, DecodingFailure}
import io.circe.generic.semiauto.{deriveEncoder, deriveDecoder}
import io.circe.refined.given
import entities.{Schedule, Entity, entityDecoder, given}
import events.Name

sealed trait Patch:
  def apply(
      schedules: Map[String Refined Name, Schedule]
  ): Map[String Refined Name, Schedule]

given Encoder[Patch] = deriveEncoder
inline given [A <: Entity](using Schedule): Decoder[Vector[A]] =
  entityDecoder[A]
given (using Schedule): Decoder[Entity] = deriveDecoder
given (using
    schedules: Map[String Refined Name, Schedule]
): Decoder[SchedulePatch] with
  def apply(c: HCursor) = for
    name <- c.keys
      .toRight(DecodingFailure("Not an object", c.history))
      .flatMap(keys =>
        keys.toSeq match
          case Nil => Left(DecodingFailure("No keys", c.history))
          case key :: Nil =>
            c.downField(key).get[String Refined Name]("schedule")
          case _ => Left(DecodingFailure("Too many keys", c.history))
      )
    given Schedule <- schedules
      .get(name)
      .toRight(DecodingFailure(s"Schedule $name not found", c.history))
    result <- deriveDecoder[SchedulePatch](c)
  yield result
given (using Map[String Refined Name, Schedule]): Decoder[Patch] = deriveDecoder

sealed trait GlobalPatch extends Patch

object GlobalPatch:
  case class Add(name: String Refined Name) extends GlobalPatch:
    def apply(schedules: Map[String Refined Name, Schedule]) =
      schedules.get(name) match
        case Some(_) => schedules
        case None    => schedules + (name -> Schedule())

  case class Remove(name: String Refined Name) extends GlobalPatch:
    def apply(schedules: Map[String Refined Name, Schedule]) =
      schedules - name

  case class SetName(oldName: String Refined Name, name: String Refined Name)
      extends GlobalPatch:
    def apply(schedules: Map[String Refined Name, Schedule]) =
      if schedules contains oldName then
        schedules + (name -> schedules(oldName))
      else schedules

sealed trait SchedulePatch extends Patch:
  val schedule: String Refined Name

  def apply(
      schedules: Map[String Refined Name, Schedule]
  ): Map[String Refined Name, Schedule] = schedules.get(schedule) match
    case Some(s) => schedules + (schedule -> patch(s))
    case None    => schedules

  def patch(s: Schedule): Schedule

object SchedulePatch:
  case class Update(schedule: String Refined Name, entity: Entity)
      extends SchedulePatch:
    def patch(s: Schedule) = s.find(_.uuid == entity.uuid) match
      case Some(prev) if prev.getClass == entity.getClass =>
        s.map(e => if e.uuid == entity.uuid then entity else e)
          .map(updateEntity(_, entity))
      case Some(prev) => Remove(schedule, entity.uuid).patch(s).appended(entity)
      case None       => s.appended(entity)

  def updateEntity(
      entity: Entity,
      updated: Entity
  ) =
    val m = summon[Mirror.SumOf[Entity]]
    matchAndUpdateEntity[m.MirroredElemTypes](entity, updated)

  inline def matchAndUpdateEntity[T <: Tuple](
      entity: Entity,
      updated: Entity
  ): Entity =
    inline erasedValue[T] match
      case _: EmptyTuple => entity
      case _: (head *: tail) =>
        if entity.isInstanceOf[head] then
          val m = summonInline[Mirror.ProductOf[head]]
          m.fromProduct(
            updateEntityInTuple(
              Tuple.fromProduct(entity.asInstanceOf[Product]),
              updated
            )
          ).asInstanceOf[Entity]
        else matchAndUpdateEntity[tail](entity, updated)

  def updateEntityInTuple(tuple: Tuple, updated: Entity): Tuple =
    tuple match
      case EmptyTuple => tuple
      case (head *: tail) =>
        head match
          case head: Vector[_] =>
            head
              .asInstanceOf[Vector[Entity]]
              .map(e =>
                if e.uuid == updated.uuid then updated else e
              ) *: updateEntityInTuple(tail, updated)
          case _ => head *: updateEntityInTuple(tail, updated)

  case class Remove(schedule: String Refined Name, uuid: UUID)
      extends SchedulePatch:
    def patch(s: Schedule) =
      s.filterNot(_.uuid == uuid)
        .map(entity =>
          val m = summon[Mirror.SumOf[Entity]]
          matchAndRemoveUUID[m.MirroredElemTypes](entity, uuid)
        )

  def removeUUID(entity: Entity, uuid: UUID) =
    val m = summon[Mirror.SumOf[Entity]]
    matchAndRemoveUUID[m.MirroredElemTypes](entity, uuid)

  inline def matchAndRemoveUUID[T <: Tuple](
      entity: Entity,
      uuid: UUID
  ): Entity =
    inline erasedValue[T] match
      case _: EmptyTuple => entity
      case _: (head *: tail) =>
        if entity.isInstanceOf[head] then
          val m = summonInline[Mirror.ProductOf[head]]
          m.fromProduct(
            removeUUIDFromTuple(
              Tuple.fromProduct(entity.asInstanceOf[Product]),
              uuid
            )
          ).asInstanceOf[Entity]
        else matchAndRemoveUUID[tail](entity, uuid)

  def removeUUIDFromTuple(tuple: Tuple, uuid: UUID): Tuple =
    tuple match
      case EmptyTuple => tuple
      case (head *: tail) =>
        head match
          case head: Vector[_] =>
            head
              .asInstanceOf[Vector[Entity]]
              .filterNot(_.uuid == uuid) *: removeUUIDFromTuple(tail, uuid)
          case _ => head *: removeUUIDFromTuple(tail, uuid)
