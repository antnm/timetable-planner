package timetableplanner
package components

import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.Refined
import entities.Schedule
import events.Name

given toggleInput: EntityComponent[Boolean] with
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      b: Boolean
  )(using Signal[Map[String, String]]) = Vector(
    span(child.text <-- messages(if b then "yes" else "no"))
  )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val toggle = input(typ := "checkbox")
    EntityComponent.Editable(
      Seq(toggle),
      toggle
        .events(onChange.mapToChecked)
        .toSignal(toggle.ref.checked)
        .map(Some(_)),
      Observer { _ => toggle.ref.checked = false },
      Observer(b => toggle.ref.checked = b)
    )
