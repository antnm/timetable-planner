package timetableplanner
package service

import cats.effect.kernel.{Async, Ref}
import cats.effect.std.Random
import cats.implicits.given
import fs2.concurrent.Topic
import doobie.util.transactor.Transactor
import org.http4s.server.middleware.GZip
import events.Event
import entities.Schedule

def app[F[_]: Async](
    random: Random[F],
    xa: Transactor[F],
    auth: Authenticator[F],
    planner: Planner[F],
    messages: Messages,
    config: Config
) =
  GZip(
    (iconRoute
      <+> javascriptRoutes
      <+> styleRoutes
      <+> messagesRoute(messages)
      <+> authenticationRoutes(xa, random, config)
      <+> timetableRoutes(planner, messages)
      <+> plannerPageRoutes
      <+> authenticatedRoutes(xa, auth, planner, random, config)).orNotFound
  )
