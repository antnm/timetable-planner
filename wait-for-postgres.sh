#!/bin/sh

set -e
  
until PGPASSWORD=timetableplanner psql -h db -U timetableplanner -d timetableplanner -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done
          
>&2 echo "Postgres is up - executing command"
exec "$@"
