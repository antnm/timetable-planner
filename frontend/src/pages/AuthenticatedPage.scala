package timetableplanner
package pages

import com.raquo.laminar.api.L.{*, given}
import com.raquo.waypoint.{Router, SplitRender}

def authenticatedPage(
    page: Signal[AuthenticatedPage]
)(using router: Router[Page])(using
    Planner,
    Var[User],
    Signal[Map[String, String]]
) =
  val splitter = SplitRender[AuthenticatedPage, HtmlElement](page)
    .collectStatic(PlannerPage)(plannerPageView)
    .collectStatic(UserPage)(userPageView)
    .collectStatic(InvitePage)(invitePageView)

  div(
    cls := "page",
    header(
      nav(
        Seq(PlannerPage, UserPage, InvitePage).map(page =>
          a(
            child.text <-- messages {
              val str = page.toString
              str.head.toLower +: str.tail
            },
            href := router.relativeUrlForPage(page),
            cls.toggle("current") <-- router.$currentPage.map(_ == page),
            composeEvents(onClick.preventDefault)(
              _.sample(router.$currentPage.map(_ != page))
                .filter(identity)
                .mapTo(page)
            ) --> router.pushState
          )
        )
      ),
      h1(img(src := "/icon.svg", alt := ""), "Timetable Planner")
    ),
    child <-- splitter.$view
  )
