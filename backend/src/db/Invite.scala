package timetableplanner
package db

import scala.concurrent.duration.DurationInt
import scala.util.Try
import java.sql.Timestamp
import java.time.{LocalDateTime, ZoneOffset}
import java.util.Base64
import cats.effect.kernel.{Async, Clock}
import cats.effect.std.Random
import cats.implicits.*
import doobie.ConnectionIO
import doobie.implicits.*
import doobie.implicits.javasql.*
import doobie.util.transactor.Transactor

class Invite private (private val bytes: Array[Byte]):
  def code = Base64.getEncoder.encodeToString(bytes)

  def isValid[F[_]: Async]: F[ConnectionIO[Boolean]] =
    for
      removeExpired <- Invite.removeExpired()
      now <- Clock[F].realTime
    yield (for
      _ <- removeExpired
      expiration <- sql"""
                      select expiration from invites
                      where code = $bytes
                    """.query[Timestamp].option
    yield expiration.exists(_.after(Timestamp(now.toMillis))))

  def remove(): ConnectionIO[Unit] =
    sql"""
      delete from invites
      where code = $bytes
    """.update.run.map(_ => ())

object Invite:
  def generate[F[_]: Async](random: Random[F]): F[ConnectionIO[Invite]] =
    for
      code <- random.nextBytes(512)
      now <- Clock[F].realTime
      expiration = Timestamp(now.plus(24.hours).toMillis)
    yield sql"""
            insert into invites (code, expiration)
            values ($code, $expiration)
          """.update.run.fmap(_ => new Invite(code))

  def apply(code: String): Option[Invite] =
    Try(Base64.getDecoder.decode(code)).toOption.map(new Invite(_))

  def removeExpired[F[_]: Async](): F[ConnectionIO[Unit]] =
    scribe.debug("Removing expired tokens")
    Clock[F].realTime.map(now => sql"""
        delete from invites
        where expiration <= ${Timestamp(now.toMillis)}
      """.update.run.fmap(_ => ()))
