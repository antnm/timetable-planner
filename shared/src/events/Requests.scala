package timetableplanner.events

import eu.timepit.refined.api.{Refined, Validate},
eu.timepit.refined.boolean.And,
eu.timepit.refined.collection.{MinSize, MaxSize}, io.circe.Codec,
io.circe.refined.given

type Name = MinSize[1] And MaxSize[128]

case class HasAllCharacterClasses()
given Validate.Plain[String, HasAllCharacterClasses] =
  Validate.fromPredicate(
    str =>
      str.exists(_.isLower) && str.exists(_.isUpper) && str.exists(_.isDigit),
    str => s"($str has all character classes)",
    HasAllCharacterClasses()
  )
type Password = HasAllCharacterClasses And MinSize[8] And MaxSize[128]

case class LoginRequest(name: String, password: String, keepLoggedIn: Boolean)
    derives Codec.AsObject

case class RegisterRequest(
    name: String Refined Name,
    password: String Refined Password,
    code: String
) derives Codec.AsObject

case class UserInfo(
    name: Option[String Refined Name],
    password: Option[String Refined Password],
    oldPassword: String
) derives Codec.AsObject
