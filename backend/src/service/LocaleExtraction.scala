package timetableplanner
package service

import org.http4s.{Request, LanguageTag}
import org.http4s.headers.`Accept-Language`

private def extractLocale(req: Request[_]) =
  req.headers.get[`Accept-Language`] match {
    case Some(header) =>
      val bestMatch = SupportedLocale.values
        .map(locale => (locale, header.qValue(LanguageTag(locale.toString))))
        .sortBy(_._2)
        .last
      if bestMatch._2.isAcceptable then bestMatch._1 else SupportedLocale.en
    case _ => SupportedLocale.en
  }
