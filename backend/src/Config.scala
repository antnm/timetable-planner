package timetableplanner

import com.comcast.ip4s.{Hostname, Port}, com.comcast.ip4s.Literals.port
import pureconfig.ConfigReader, pureconfig.generic.derivation.default.derived
import scala.concurrent.duration.FiniteDuration

implicit val logLevelReader: ConfigReader[scribe.Level] =
  ConfigReader[String].map(scribe.Level.get(_).get)
implicit val hostnameReader: ConfigReader[Hostname] =
  ConfigReader[String].map(Hostname.fromString(_).get)
implicit val portReader: ConfigReader[Port] =
  ConfigReader[Int].map(Port.fromInt(_).get)

case class Config(
    host: Hostname,
    port: Port,
    logLevel: scribe.Level,
    session: SessionConfig,
    solver: SolverConfig,
    database: DatabaseConfig,
    topic: TopicConfig
) derives ConfigReader

case class SessionConfig(
    expiration: FiniteDuration,
    codeSize: Int
)

case class SolverConfig(
    minutesSpentLimit: Long,
    unimprovedMinutesSpentLimit: Long
)

case class DatabaseConfig(
    url: String,
    user: String,
    password: String
) derives ConfigReader

case class TopicConfig(
    eventsMaxQueue: Int,
    dbUpdatesMaxQueue: Int
)
