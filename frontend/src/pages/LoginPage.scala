package timetableplanner
package pages

import java.net.HttpURLConnection.{HTTP_OK, HTTP_UNAUTHORIZED}
import org.scalajs.dom.HttpMethod.POST
import eu.timepit.refined.boolean.True
import com.raquo.laminar.api.L.{*, given}
import com.raquo.waypoint.Router
import components.{error, ValidatedInput, given}
import events.LoginRequest

def loginPage(using router: Router[Page], user: Var[User])(using
    Signal[Map[String, String]]
) =
  val waitingRequest = Var(false)
  val loginError = Var[Option[String]](None)
  val name = ValidatedInput[True](
    defaultValue = "",
    invalidMessage = "",
    disabled <-- waitingRequest,
    onInput.mapTo(None) --> loginError,
    loginError --> error
  )
  val password = ValidatedInput[True](
    defaultValue = "",
    invalidMessage = "",
    typ := "password",
    disabled <-- waitingRequest,
    onInput.mapTo(None) --> loginError,
    loginError --> error
  )
  val keepLoggedInCheck = input(typ := "checkbox")

  div(
    cls := "page",
    header(h1(img(src := "/icon.svg", alt := ""), "Timetable Planner")),
    main(
      h2(child.text <-- messages("loginTitle")),
      form(
        label(child.text <-- messages("name"), name),
        label(child.text <-- messages("password"), password),
        label(child.text <-- messages("keepLoggedIn"), keepLoggedInCheck),
        button(
          typ := "button",
          child.text <-- messages("login"),
          disabled <-- loginError.signal.combineWithFn(waitingRequest.signal)(
            _.isDefined || _
          ),
          inContext(
            _.events(onClick)
              .flatMap(_ =>
                waitingRequest set true
                user set User(name.ref.value)
                fetch(
                  POST,
                  "api/login",
                  LoginRequest(
                    name.ref.value,
                    password.ref.value,
                    keepLoggedInCheck.ref.checked
                  )
                )
              )
              .map(_.status) --> {
              case HTTP_OK =>
                scribe.debug("Login successful")
                router.pushState(PlannerPage)
              case HTTP_UNAUTHORIZED =>
                scribe.debug("Login unauthorized")
                loginError set Some("invalidCredentials")
                name.report onNext ()
                waitingRequest set false
              case _ =>
                scribe.warn("Unknown login status code")
                loginError set Some("loginFail")
                name.report onNext ()
                waitingRequest set false
            }
          )
        ),
        a(
          child.text <-- messages("createAccount"),
          href := "/register",
          onClick.preventDefault.mapTo(RegisterPage) --> router.pushState
        )
      )
    )
  )
