package timetableplanner

import org.scalajs.dom.document.querySelector
import com.raquo.laminar.api.L.{*, given}
import org.scalajs.dom.HttpMethod.GET
import com.raquo.waypoint.Router

@main def main =
  scribe.Logger.root
    .clearHandlers()
    .clearModifiers()
    .withHandler(minimumLevel =
      Some(
        if scalajs.LinkingInfo.developmentMode
        then scribe.Level.Debug
        else scribe.Level.Error
      )
    )
    .replace()
  given Var[User] = Var(User())
  given planner: Planner = Planner()
  val messages = Var[Map[String, String]](Map.WithDefault(Map.empty, _ => ""))
  given router: Router[Page] = mkRouter(messages.signal)
  if router.$currentPage.now().isInstanceOf[AuthenticatedPage] then
    planner.connect onNext ()
  given Signal[Map[String, String]] = messages.signal
  val split = mkSplitter
  val root = div(
    messages.signal.changes --> (_ =>
      router.forcePage(router.$currentPage.now())
    ),
    fetch(GET, s"/api/messages/$documentLang")
      .flatMap(_.data[Map[String, String]]) --> messages,
    planner.bindings,
    planner.open.map(_ =>
      scribe.debug("EventSource connected")
      val current = router.$currentPage.now()
      if current.isInstanceOf[AuthenticatedPage] then current else LoginPage
    ) --> router.pushState,
    planner.error.mapTo(LoginPage) --> router.pushState,
    router.$currentPage.changes
      .filter(!_.isInstanceOf[AuthenticatedPage])
      .mapTo(()) --> planner.disconnect,
    router.$currentPage.changes
      .filter(_.isInstanceOf[AuthenticatedPage])
      .mapTo(()) --> planner.connect,
    child <-- split.$view
  )
  renderOnDomContentLoaded(querySelector("body"), root)

case class User(name: String = "")
