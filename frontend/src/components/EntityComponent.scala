package timetableplanner
package components

import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import entities.Schedule

trait EntityComponent[T]:
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      t: T
  )(using Signal[Map[String, String]]): Children
  def edit(s: Signal[Schedule])(using
      Signal[Map[String, String]]
  ): EntityComponent.Editable[T]

object EntityComponent:
  case class Editable[T](
      children: Children,
      current: Signal[Option[T]],
      clear: Observer[Unit],
      selected: Observer[T]
  )

given EntityComponent[EmptyTuple] with
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      empty: EmptyTuple
  )(using Signal[Map[String, String]]) = Seq()

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    EntityComponent.Editable(
      Vector(),
      Signal.fromValue(Some(EmptyTuple)),
      Observer.empty,
      Observer.empty
    )

given [H: EntityComponent, T <: Tuple: EntityComponent]: EntityComponent[H *: T]
  with
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      tuple: H *: T
  )(using Signal[Map[String, String]]) =
    summon[EntityComponent[H]]
      .show(uuid, s, tuple.head) ++ summon[EntityComponent[T]].show(
      uuid,
      s,
      tuple.tail
    )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val head = summon[EntityComponent[H]].edit(s)
    val tail = summon[EntityComponent[T]].edit(s)
    EntityComponent.Editable(
      head.children ++ tail.children,
      head.current.combineWithFn(tail.current) {
        case (Some(head), Some(tail)) => Some(head *: tail)
        case _                        => None
      },
      Observer(_ =>
        head.clear onNext ()
        tail.clear onNext ()
      ),
      Observer { case (selectedHead *: selectedTail) =>
        head.selected onNext selectedHead
        tail.selected onNext selectedTail
      }
    )

given uuidComp[T <: Tuple: EntityComponent]: EntityComponent[UUID *: T] with
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      tuple: UUID *: T
  )(using Signal[Map[String, String]]) =
    summon[EntityComponent[T]].show(uuid, s, tuple.tail)

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val tail = summon[EntityComponent[T]].edit(s)
    val uuid = Var(UUID.randomUUID())
    EntityComponent.Editable(
      tail.children,
      uuid.signal.combineWithFn(tail.current) {
        case (uuid, Some(tail)) => Some(uuid *: tail)
        case _                  => None
      },
      Observer(_ =>
        uuid set UUID.randomUUID()
        tail.clear onNext ()
      ),
      Observer { case (selectedUuid *: selectedTail) =>
        uuid set selectedUuid
        tail.selected onNext selectedTail
      }
    )
