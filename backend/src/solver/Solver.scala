package timetableplanner
package solver

import scala.jdk.CollectionConverters.given
import scala.util.{Try, Success, Failure}
import eu.timepit.refined.auto.given, eu.timepit.refined.api.Refined
import org.optaplanner.core.api.solver.SolverFactory
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig
import org.optaplanner.core.config.solver.SolverConfig
import org.optaplanner.core.config.solver.termination.TerminationConfig
import entities.{Schedule, Timetable, TimeOfDay}
import entities.Entity.{Class, Teacher, Subject}

class Solver(config: Config):
  private val solver =
    SolverFactory
      .create[Solution](
        SolverConfig()
          .withSolutionClass(classOf[Solution])
          .withEntityClasses(classOf[Entity])
          .withScoreDirectorFactory(
            ScoreDirectorFactoryConfig()
              .withConstraintProviderClass(classOf[Constraints])
          )
          .withTerminationConfig(
            TerminationConfig()
              .withMinutesSpentLimit(config.solver.minutesSpentLimit)
              .withUnimprovedMinutesSpentLimit(
                config.solver.unimprovedMinutesSpentLimit
              )
              .withBestScoreLimit("0hard/0soft")
          )
      )
      .buildSolver

  def solve(schedule: Schedule): Option[Timetable] =
    Try(solver.solve(Solution(schedule))) match
      case Success(solution) =>
        val entries = solution.entries
        scribe.debug(s"Solved with solution $entries")
        Some(
          entries.asScala
            .to(Set)
            .map(c =>
              val start = TimeOfDay(
                hour = Refined.unsafeApply(c.minute % (24 * 60) / 60),
                minute = Refined.unsafeApply(c.minute % 60)
              )
              Timetable.Entry(
                `class` =
                  if (c.`class`._1.count: Int) == 1 then c.`class`._1.name
                  else
                    Refined.unsafeApply(s"${c.`class`._1.name}${c.`class`._2}")
                ,
                teacher = c.teacher.name,
                subject = c.subject.name,
                start = start,
                end = TimeOfDay(
                  hour = Refined.unsafeApply(
                    (start.hour + c.length / 60 + (start.minute + c.length % 60) / 60) % 24
                  ),
                  minute =
                    Refined.unsafeApply((start.minute + c.length % 60) % 60)
                ),
                day = Refined.unsafeApply(c.minute / (24 * 60)),
                startingFromWeek = Refined.unsafeApply(c.startingFromWeek),
                weekFrequency = Refined.unsafeApply(c.weekFrequency),
                classroom = c.classroom.map(_.name)
              )
            )
        )
      case Failure(e) =>
        scribe.warn(s"Could not solve: ${e.getMessage}")
        None
