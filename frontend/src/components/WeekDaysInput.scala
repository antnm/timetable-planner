package timetableplanner
package components

import scala.util.Try
import scala.scalajs.js.{Array, Dictionary, Date}
import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.api.{Refined, Validate}
import entities.{Schedule, DayOfTheWeek}

given weekDaysInput: EntityComponent[Set[Int Refined DayOfTheWeek]] with
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      d: Set[Int Refined DayOfTheWeek]
  )(using Signal[Map[String, String]]) = Vector(
    span(
      Intl
        .ListFormat(documentLang, Dictionary.empty)
        .format(
          Array(
            d.toVector
              .sortBy(d => d: Int)
              .map(d => weekDayInput.format(d))*
          )
        )
    )
  )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val current = Var[Set[Int Refined DayOfTheWeek]](Set())
    val children = Vector(
      ul(
        (0 until 7)
          .map(Refined.unsafeApply[Int, DayOfTheWeek])
          .toVector
          .map(d =>
            li(
              label(
                input(
                  typ := "checkbox",
                  controlled(
                    checked <-- current.signal.map(_ contains d),
                    onClick.mapToChecked.map(checked =>
                      if checked then current.now() union Set(d)
                      else current.now() diff Set(d)
                    ) --> current
                  )
                ),
                weekDayInput.format(d)
              )
            )
          )
      )
    )
    EntityComponent.Editable(
      children,
      current.signal.map(Some(_)),
      Observer(_ => current set Set()),
      current.toObserver
    )
