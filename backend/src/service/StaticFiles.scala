package timetableplanner
package service

import cats.effect.kernel.Async
import cats.implicits.*
import org.http4s.{HttpRoutes, StaticFile}
import org.http4s.dsl.Http4sDsl

private def iconRoute[F[_]: Async] =
  val dsl = Http4sDsl[F]
  import dsl.*
  HttpRoutes.of[F] { case req @ GET -> Root / "icon.svg" =>
    StaticFile
      .fromResource[F]("icon.svg", Some(req))
      .getOrElseF(NotFound())
  }

private def styleRoutes[F[_]: Async] =
  val dsl = Http4sDsl[F]
  import dsl.*
  Seq("common.css", "webapp.css")
    .map(css =>
      HttpRoutes.of[F] {
        case req @ GET -> Root / path if path == css =>
          StaticFile
            .fromResource[F](css, Some(req))
            .getOrElseF(NotFound())
      }
    )
    .reduce(_ <+> _)

private def javascriptRoutes[F[_]: Async] =
  val dsl = Http4sDsl[F]
  import dsl.*
  Seq("out.js", "out.js.map")
    .map(js =>
      HttpRoutes.of[F] {
        case req @ GET -> Root / path if path == js =>
          StaticFile.fromResource[F](js, Some(req)).getOrElseF(NotFound())
      }
    )
    .reduce(_ <+> _)
