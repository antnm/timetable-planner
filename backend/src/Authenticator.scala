package timetableplanner

import scala.concurrent.duration.Deadline
import java.util.Base64
import cats.data.{Kleisli, OptionT}
import cats.effect.kernel.{Async, Ref}
import cats.effect.std.Random
import cats.implicits.*
import doobie.implicits.*
import doobie.util.transactor.Transactor
import org.http4s.headers.Cookie
import org.http4s.{Request, ResponseCookie, SameSite}
import org.http4s.server.AuthMiddleware
import db.{Session, User}

class Authenticator[F[_]: Async](
    xa: Transactor[F],
    random: Random[F],
    config: Config
):
  def middleware: AuthMiddleware[F, (User, ResponseCookie)] =
    AuthMiddleware(
      Kleisli((request: Request[F]) =>
        ((for
          header <- request.headers.get[Cookie]
          cookie <- header.values.toList.find(_.name == "session")
          session <- Session(cookie.content, config)
        yield OptionT(
          (session
            .use(random, config)
            .flatMap(_.transact(xa))
            .map(
              _.map((user, session, keepLoggedIn) =>
                (user, Authenticator.cookie(config, session, keepLoggedIn))
              )
            ))
        )).getOrElse(OptionT.none))
      )
    )

object Authenticator:
  def cookie(config: Config, session: Session, keepLoggedIn: Boolean) =
    ResponseCookie(
      name = "session",
      content = session.code,
      maxAge = Option.when(keepLoggedIn)(config.session.expiration.toSeconds),
      path = Some("/api"),
      sameSite = Some(SameSite.Strict),
      httpOnly = true
    )
