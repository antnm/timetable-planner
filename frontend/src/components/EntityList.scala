package timetableplanner
package components

import scala.deriving.Mirror
import scala.compiletime.{constValue, constValueTuple, summonInline}
import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import entities.Entity
import events.SchedulePatch

inline def entityList[T <: Entity & Product](using
    planner: Planner,
    m: Mirror.ProductOf[T]
)(using Signal[Map[String, String]]) =
  val labels = constValueTuple[m.MirroredElemLabels].productIterator
    .asInstanceOf[Iterator[String]]
    .toVector
    .filterNot(_ == "uuid")
  val comp = summonInline[EntityComponent[m.MirroredElemTypes]]
  val compEdit = comp.edit(planner.schedule)
  val current = compEdit.current.map(_.map(m.fromProduct(_)))
  val editing = Var[Option[UUID]](None)
  val modal = dialog(
    dialog.close.mapTo(Option.empty[UUID]) --> editing,
    dialog.close.mapTo(()) --> compEdit.clear,
    labels
      .zip(compEdit.children)
      .map((l, e) => label(child.text <-- messages(l), e))
  )
  modal.amend(
    button(
      typ := "button",
      child.text <-- messages("close"),
      onClick.mapTo("") --> modal.ref.close
    ),
    button(
      typ := "button",
      child.text <-- messages("save"),
      disabled <-- current.map(_.isEmpty),
      composeEvents(onClick)(_.sample(current, editing).collect {
        case (Some(c), editing) =>
          SchedulePatch.Update(planner.currentSchedule.now().get, c)
      }) --> (p =>
        planner.patchSchedules onNext p
        modal.ref.close("")
      )
    )
  )
  val plural = s"${constValue[m.MirroredLabel].head.toLower}${constValue[
      m.MirroredLabel
    ].tail}${if constValue[m.MirroredLabel].endsWith("s") then "es" else "s"}"
  details(
    summary(
      child.text <-- messages(plural),
      details(
        summary("?"),
        div(child.text <-- messages(plural ++ "Help"))
      )
    ),
    ul(
      children <-- planner.schedule
        .map(_.collect { case component: T => component }.toSeq)
        .split(_.uuid)((uuid, _, stream) =>
          li(
            dl(children <-- stream.combineWithFn(planner.schedule) {
              case (c, s) =>
                labels
                  .zip(
                    comp.show(
                      uuid,
                      planner.schedule,
                      Tuple.fromProductTyped(c)
                    )
                  )
                  .map((label, value) =>
                    Seq(dt(child.text <-- messages(label)), dd(value))
                  )
                  .flatten
                  .toSeq
            }),
            button(
              typ := "button",
              child.text <-- messages("edit"),
              composeEvents(onClick)(_.sample(stream)) --> { case c =>
                compEdit.selected onNext Tuple.fromProductTyped(c)
                editing set Some(uuid)
                modal.ref.showModal()
              }
            ),
            button(
              typ := "button",
              child.text <-- messages("remove"),
              onClick
                .mapTo(
                  SchedulePatch
                    .Remove(planner.currentSchedule.now().get, uuid)
                ) --> planner.patchSchedules
            )
          )
        )
    ),
    modal,
    button(
      typ := "button",
      child.text <-- messages("add"),
      onClick --> (_ => modal.ref.showModal())
    )
  )
