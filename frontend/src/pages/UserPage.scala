package timetableplanner
package pages

import eu.timepit.refined.auto.given
import eu.timepit.refined.refineV
import eu.timepit.refined.boolean.{True, Or}
import eu.timepit.refined.collection.Empty
import com.raquo.laminar.api.L.{*, given}
import com.raquo.laminar.nodes.ReactiveHtmlElement
import org.scalajs.dom.HttpMethod.POST
import components.{ValidatedInput, error, given}
import events.{UserInfo, Name, Password, given}

def userPageView(using user: Var[User])(using Signal[Map[String, String]]) =
  val waitingRequest = Var(false)
  val requestError = Var[Option[String]](None)
  val name = ValidatedInput[Name](
    defaultValue = user.now().name,
    invalidMessage = "nameRequirements",
    disabled <-- waitingRequest
  )
  val password = ValidatedInput[Password Or Empty](
    defaultValue = "",
    invalidMessage = "passwordRequirements",
    typ := "password"
  )
  val noChangeError = Signal
    .combine(
      name.events(onInput).startWith(()).mapTo(name.ref.value),
      password.events(onInput).startWith(()).mapTo(password.ref.value)
    )
    .map((name, password) =>
      if name != user.now().name || password.nonEmpty then None
      else Some("noNameOrPasswordChange")
    )
  val confirmPassword =
    ValidatedInput[True](
      defaultValue = "",
      invalidMessage = "",
      typ := "password",
      disabled <-- waitingRequest
    )
  val noMatchError = Signal
    .combine(
      password.events(onInput).startWith(()).mapTo(password.ref.value),
      confirmPassword
        .events(onInput)
        .startWith(())
        .mapTo(confirmPassword.ref.value)
    )
    .map((password, confirm) =>
      if password == confirm then None else Some("passwordNotMatching")
    )
  val currentPassword = input(typ := "password", disabled <-- waitingRequest)
  val hasError = Signal
    .combineSeq(
      Seq(
        name.error.signal,
        password.error.signal,
        requestError.signal,
        noChangeError,
        noMatchError
      )
    )
    .map(_.exists(_.isDefined))

  main(
    idAttr := "user-info",
    legend(h2(child.text <-- messages("changeUserInformation"))),
    label(
      child.text <-- messages("name"),
      name,
      noChangeError --> name.error,
      requestError.signal.changes.collect { case Some(message) =>
        message
      } --> (message =>
        name.error set Some(message)
        name.report onNext ()
      )
    ),
    label(
      child.text <-- messages("newPassword"),
      password,
      noChangeError --> password.error
    ),
    label(
      child.text <-- messages("confirmNewPassword"),
      confirmPassword,
      noMatchError --> confirmPassword.error
    ),
    label(child.text <-- messages("currentPassword"), currentPassword),
    button(
      typ := "button",
      child.text <-- messages("changeInformation"),
      disabled <-- hasError.combineWithFn(waitingRequest.signal)(_ || _),
      composeEvents(onClick)(
        _.sample(name.validated.combineWith(password.validated))
          .collect { case (Some(name), Some(password)) =>
            (name, password, currentPassword.ref.value)
          }
          .flatMap { case (name, password, currentPassword) =>
            waitingRequest set true
            fetch(
              POST,
              "/api/user-info",
              UserInfo(
                name =
                  if (name: String) == user.now().name then None
                  else Some(name),
                password =
                  if password.isBlank then None
                  else Some(refineV[Password](password: String).toOption.get),
                oldPassword = currentPassword
              )
            ).map(_.ok)
          }
      ) --> (ok =>
        if ok then
          user set User(name.ref.value)
          currentPassword.ref.value = ""
        else requestError set Some("requestFailed")
        waitingRequest set false
      )
    )
  )
