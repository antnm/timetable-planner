create table sessions (
  user_id    int       not null,
  expiration timestamp not null,
  code       bytea     not null,
  constraint fk_user
    foreign key(user_id)
    references users(id)
    on delete cascade
);
