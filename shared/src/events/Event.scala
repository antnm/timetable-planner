package timetableplanner
package events

import eu.timepit.refined.api.Refined
import io.circe.{Encoder, Decoder}
import io.circe.generic.semiauto.{deriveEncoder, deriveDecoder}
import io.circe.refined.given
import entities.{Schedule, Timetable, State, given}
import events.{Name, given}

enum Event:
  case Initial(state: State)
  case Patch(userId: Int, patch: timetableplanner.events.Patch)
  case ConfirmPatch
  case Solving(name: String Refined Name)
  case Solved(name: String Refined Name, timetable: Option[Timetable])
  case Published(name: String Refined Name, published: Boolean)

given Encoder[Event] = deriveEncoder
given (using Map[String Refined Name, Schedule]): Decoder[Event] = deriveDecoder
