package timetableplanner
package components

import scala.util.Try
import scala.scalajs.js.{Dictionary, Date}
import java.util.UUID
import com.raquo.laminar.api.L.{*, given}
import eu.timepit.refined.auto.given
import eu.timepit.refined.refineV
import eu.timepit.refined.api.{Refined, Validate}
import eu.timepit.refined.numeric.NonNegative
import entities.{
  Schedule,
  TimeOfDay,
  TimeOfDayInterval,
  NonNegativeIntRange,
  given
}

case class ValidNonNegativeInt()
given Validate.Plain[String, ValidNonNegativeInt] =
  Validate.fromPredicate(
    str => Try(str.toInt).toOption.filter(_ >= 0).isDefined,
    str => s"$str is a valid nonnegative int",
    ValidNonNegativeInt()
  )

given nonNegativeIntRangeInput: EntityComponent[
  (Int Refined NonNegative, Int Refined NonNegative) Refined NonNegativeIntRange
] with
  def show(
      uuid: UUID,
      s: Signal[Schedule],
      r: (
          Int Refined NonNegative,
          Int Refined NonNegative
      ) Refined NonNegativeIntRange
  )(using Signal[Map[String, String]]) = Vector(
    span(
      {
        val numberFormat = Intl.NumberFormat(documentLang, Dictionary.empty)
        f"${numberFormat.format(r._1)}-${numberFormat.format(r._2)}"
      }
    )
  )

  def edit(s: Signal[Schedule])(using Signal[Map[String, String]]) =
    val start =
      ValidatedInput[ValidNonNegativeInt](
        "",
        "nonNegativeIntRequirement",
        typ := "number"
      )
    val end =
      ValidatedInput[ValidNonNegativeInt](
        "",
        "nonNegativeIntRequirement",
        typ := "number"
      )
    EntityComponent.Editable(
      Seq(div(cls := "interval", start, "-", end)),
      start.validated.combineWithFn(end.validated) {
        case (Some(start), Some(end)) =>
          refineV[NonNegativeIntRange](
            (
              Refined.unsafeApply((start: String).toInt),
              Refined.unsafeApply((end: String).toInt)
            )
          ).toOption
        case _ => None
      },
      Observer(_ =>
        start.clear onNext ()
        end.clear onNext ()
      ),
      Observer(r =>
        start.value set r._1.toString
        end.value set r._2.toString
      )
    )
