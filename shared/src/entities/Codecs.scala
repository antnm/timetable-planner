package timetableplanner
package entities

import scala.annotation.tailrec
import scala.deriving.Mirror
import scala.compiletime.{erasedValue, summonFrom}
import scala.compiletime.constValueTuple
import java.util.UUID
import io.circe.{Json, Codec, Encoder, Decoder, HCursor, DecodingFailure}
import io.circe.syntax.given
import io.circe.refined.given
import io.circe.generic.semiauto.{deriveEncoder, deriveDecoder}

val entityEncoder =
  def idEncoder[A <: Entity] = new Encoder[Vector[A]]:
    def apply(c: Vector[A]) = Json.arr(c.map(_.uuid.asJson)*)
  given [A <: Entity]: Encoder[Vector[A]] = idEncoder
  deriveEncoder[Entity]
given Encoder[Entity] = entityEncoder
given Encoder[Schedule] = Encoder.encodeVector(using entityEncoder)

inline def entityDecoder[A <: Entity](using schedule: Schedule) =
  summonFrom { case m @ given Mirror.SumOf[Entity] =>
    Decoder.decodeVector(using
      matchAndMkEntityDecoder[m.MirroredElemTypes, A](schedule)
    )
  }

inline def matchAndMkEntityDecoder[T <: Tuple, A <: Entity](
    schedule: Schedule
): Decoder[A] =
  inline erasedValue[T] match
    case _: (head *: tail) =>
      inline erasedValue[A] match
        case _: head =>
          new Decoder[A]:
            def apply(c: HCursor) = for
              id <- c.as[UUID]
              value <- schedule
                .find(c => c.uuid == id && c.isInstanceOf[head])
                .map(_.asInstanceOf[A])
                .toRight(DecodingFailure("UUID not found", c.history))
            yield value
        case _ => matchAndMkEntityDecoder[tail, A](schedule)

given Decoder[Schedule] with
  def apply(c: HCursor) =
    val m = summon[Mirror.SumOf[Entity]]
    val labels = constValueTuple[m.MirroredElemLabels].productIterator.toVector
      .asInstanceOf[Vector[String]]
    c.values match
      case Some(iter) =>
        val vec = iter.toVector
        if vec.forall(
            _.asObject.map(o =>
              o.size == 1 && (labels contains o.keys.head)
            ) getOrElse false
          )
        then decodeEntities(labels, vec, Vector())
        else Left(DecodingFailure("Invalid array", c.history))
      case None => Left(DecodingFailure("Not an array", c.history))

  @tailrec
  def decodeEntities(
      labels: Vector[String],
      vec: Vector[Json],
      prev: Schedule
  ): Decoder.Result[Schedule] =
    inline given [A <: Entity]: Decoder[Vector[A]] =
      entityDecoder[A](using prev)
    val decoder = deriveDecoder[Entity]
    labels.headOption match
      case None => Right(prev)
      case Some(head) =>
        val results =
          vec.filter(_.asObject.get contains head).map(decoder decodeJson _)
        results.collectFirst { case Left(r) => r } match
          case Some(r) => Left(r)
          case None =>
            val newPrev = prev ++ results.collect { case Right(r) => r }
            decodeEntities(labels.tail, vec, newPrev)
