package timetableplanner
package components

import eu.timepit.refined.api.{Refined, Validate}, eu.timepit.refined.refineV
import com.raquo.laminar.api.L.{*, given}
import com.raquo.laminar.nodes.ReactiveHtmlElement
import org.scalajs.dom.html.Input

def error(using input: ValidatedInput[_]) = input.error
def report(using input: ValidatedInput[_]) = input.report
def clearInput(using input: ValidatedInput[_]) = input.clear

class ValidatedInput[A](
    defaultValue: => String,
    invalidMessage: String,
    modifiers: (ValidatedInput[A] ?=> Modifier[ReactiveHtmlElement[Input]])*
)(using messages: Signal[Map[String, String]])(using Validate[String, A]):
  private val reportBus = EventBus[Unit]()
  val report = reportBus.toObserver
  private val clearBus = EventBus[Unit]()
  val clear = clearBus.toObserver
  val value = Var(defaultValue)
  private val validatedWithError =
    value.signal.map(refineV(_).left.map(_ => invalidMessage))
  val validated = validatedWithError.map(_.toOption)
  val error = Var[Option[String]](None)

  val elem = input(
    controlled(
      com.raquo.laminar.api.L.value <-- value,
      onInput.mapToValue --> value
    ),
    validatedWithError.map(_.left.toOption) --> error,
    inContext(elem =>
      error.signal
        .combineWithFn(messages) {
          case (Some(message), messages) => Some(messages(message))
          case (None, _)                 => Some("")
        } --> (_.foreach(elem.ref setCustomValidity _))
    ),
    inContext(elem =>
      Seq(
        // Reporting right after setCustomValidity sometimes doesn't work
        elem.events(onChange).debounce(0),
        reportBus.events.debounce(0)
      ).map(
        _ --> (_ =>
          elem.ref
            .asInstanceOf[scala.scalajs.js.Dynamic]
            .applyDynamic("reportValidity")()
        )
      )
    ),
    clearBus.events --> (_ => value set defaultValue),
    modifiers.map(_(using this))
  )

given Conversion[ValidatedInput[_], ReactiveHtmlElement[Input]] with
  def apply(validated: ValidatedInput[_]) = validated.elem
