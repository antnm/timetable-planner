package timetableplanner

import scala.concurrent.Await
import scala.concurrent.duration.*
import com.raquo.laminar.api.L.*
import io.circe.{Decoder, Encoder}
import io.circe.syntax.EncoderOps
import io.circe.scalajs.decodeJs
import org.scalajs.dom.{
  fetch => jsFetch,
  HttpMethod,
  RequestInit,
  Response => RawResponse
}

class Response(response: RawResponse):
  val ok = response.ok
  val status = response.status
  def data[T](implicit d: Decoder[T]) =
    EventStream
      .fromJsPromise(response.json())
      .map(json =>
        val decoded = decodeJs(json)
        decoded.left.foreach(err =>
          scribe.error(s"Failed to decode fetch response: $err")
        )
        decoded.toOption.get
      )

private def fetchWithInit(path: String, init: RequestInit) =
  EventStream.fromJsPromise(jsFetch(path, init)).map(Response(_))

def fetch[T](method: HttpMethod, path: String, body: T)(implicit
    encoder: Encoder[T]
) =
  object init extends RequestInit
  init.method = method
  init.body = body.asJson.noSpaces
  fetchWithInit(path, init)

def fetch(method: HttpMethod, path: String) =
  object init extends RequestInit
  init.method = method
  fetchWithInit(path, init)
