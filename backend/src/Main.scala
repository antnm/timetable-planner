package timetableplanner

import scala.concurrent.duration.{MILLISECONDS, NANOSECONDS}
import cats.syntax.parallel.given
import cats.effect.{ExitCode, IO, IOApp}
import cats.effect.kernel.Ref
import cats.effect.std.Random
import concurrent.duration.*
import doobie.util.transactor.Transactor
import fs2.Stream
import fs2.concurrent.Topic
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits.given
import pureconfig.ConfigSource
import scribe.modify.LevelFilter

object App extends IOApp:
  def run(args: List[String]) =
    val config = ConfigSource.default.loadOrThrow[Config]
    scribe.Logger.root
      .clearHandlers()
      .withHandler(minimumLevel = Some(config.logLevel))
      .withModifier(
        scribe.filter
          .select(scribe.filter.packageName.startsWith("org.drools"))
          .exclude(scribe.filter.level < scribe.Level.Warn)
          .priority(scribe.Priority.High)
      )
      .replace()
    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      config.database.url,
      config.database.user,
      config.database.password
    )
    for
      planner <- Planner[IO](xa, config)
      dbUpdates = planner.dbUpdates
      messages <- messages[IO]
      random <- Random.javaSecuritySecureRandom[IO]
      auth = Authenticator(xa, random, config)
      code <- Stream
        .bracket(
          EmberServerBuilder
            .default[IO]
            .withHost(config.host)
            .withPort(config.port)
            .withIdleTimeout(Duration.Inf)
            .withHttpApp(
              service.app[IO](random, xa, auth, planner, messages, config)
            )
            .build
            .use(_ => IO.never)
            .as(ExitCode.Success)
        )(_ => IO.pure(()))
        .merge(dbUpdates)
        .compile
        .drain
    yield ExitCode.Success
