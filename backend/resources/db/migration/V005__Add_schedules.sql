create table schedules (
  name    text  primary key,
  content jsonb not null
);
