package timetableplanner

import scala.jdk.CollectionConverters.given
import java.util.{Locale, ResourceBundle}
import cats.effect.Async, cats.implicits.given

type Messages = Map[SupportedLocale, Map[String, String]]

def messages[F[_]: Async]: F[Messages] = SupportedLocale.values.toList
  .map(locale =>
    for
      resourceBundle <- Async[F].blocking(
        ResourceBundle.getBundle("Messages", Locale(locale.toString))
      )
      messages = resourceBundle.getKeys.asScala
        .map(key => (key, resourceBundle.getString(key)))
        .toMap
    yield (locale, messages)
  )
  .sequence
  .map(_.toMap)
