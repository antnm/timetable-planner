package timetableplanner

import org.scalajs.dom.document.documentElement
import com.raquo.laminar.api.L.*

def messages(message: String)(using m: Signal[Map[String, String]]) =
  m.map(_(message))

val documentLang = documentElement.getAttribute("lang")
