package timetableplanner
package service

import cats.effect.kernel.Async
import cats.effect.std.Random
import cats.implicits.given
import eu.timepit.refined.api.Refined
import doobie.util.transactor.Transactor
import doobie.implicits.given
import org.http4s.{AuthedRoutes, ServerSentEvent, Response}
import org.http4s.dsl.Http4sDsl
import org.http4s.circe.CirceEntityCodec.given
import io.circe.refined.given
import io.circe.syntax.given
import solver.Solver
import entities.Schedule
import events.{Event, Patch, Name, UserInfo, given}
import db.Invite

private def authenticatedRoutes[F[_]: Async](
    xa: Transactor[F],
    auth: Authenticator[F],
    planner: Planner[F],
    random: Random[F],
    config: Config
) =
  val dsl = Http4sDsl[F]
  import dsl.*
  auth.middleware(AuthedRoutes.of {
    case (req @ PUT -> Root / "api" / "planner") as (user, cookie) =>
      for
        given Map[String Refined Name, Schedule] <- planner.schedules
        patch <- req.as[Patch]
        _ <- planner.patch(Event.Patch(user.id, patch))
        response <- Ok()
      yield response.addCookie(cookie)
    case GET -> Root / "api" / "planner" as (user, cookie) =>
      for
        events <- planner.events
        response <- Ok(
          events.map(event =>
            ServerSentEvent(Some((event match
              case Event.Patch(id, _) if id == user.id => Event.ConfirmPatch
              case e                                   => e
            ).asJson.noSpaces))
          )
        )
      yield response.addCookie(cookie)
    case (req @ POST -> Root / "api" / "solve") as (user, cookie) =>
      for
        name <- req.as[String Refined Name]
        success <- planner.solve(name)
        response <- if success then Ok() else NotFound()
      yield response.addCookie(cookie)
    case (req @ POST -> Root / "api" / "publish") as (user, cookie) =>
      for
        schedules <- planner.schedules
        tuple <- req.as[(String Refined Name, Boolean)]
        (name, published) = tuple
        success <- planner.setPublished(name, published)
        response <- if success then Ok() else NotFound()
      yield response.addCookie(cookie)
    case (req @ POST -> Root / "api" / "user-info") as (user, cookie) =>
      for
        userInfo <- req.as[UserInfo]
        changed <- user.changeInfo(userInfo).transact(xa)
      yield Response(if changed then Ok else Unauthorized).addCookie(cookie)
    case (req @ PUT -> Root / "api" / "invite") as (user, cookie) =>
      for
        generate <- Invite.generate(random)
        invite <- generate.transact(xa)
        response <- Ok(invite.code.asJson)
      yield response.addCookie(cookie)
  })
